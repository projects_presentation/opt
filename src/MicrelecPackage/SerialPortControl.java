/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MicrelecPackage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import javax.comm.CommPortIdentifier;
import javax.comm.SerialPort;
import javax.comm.UnsupportedCommOperationException;

public class SerialPortControl {

    // <editor-fold defaultstate="collapsed" desc="Constants">
    public static final byte STX = 0x02;
    public static final byte ETX = 0x03;
    public static final byte EOT = 0x04;
    public static final byte ENQ = 0x05;
    public static final byte ACK = 0x06;
    public static final byte NAK = 0x15;
    public static final byte P = 0x50;
    public static final byte S = 0x53;
    public static final byte A = 0x41;
    public static final byte N = 0x4E;
    public static final byte E = 0x45;
    public static final byte T = 0x54;
    public static final byte m = 0x6D;
    private final int RECEIVE_TIMEOUT = 500;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Variables">
    private SerialPort serialPort;
    private InputStream inputStream = null;
    private OutputStream outputStream = null;// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Constructor">
    public SerialPortControl(String serialPortName, int baudrate, int stopBits, int parity, int databits) {
        OpenPort(serialPortName);
        try {
            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
            serialPort.setSerialPortParams(baudrate, databits, stopBits, parity);
        } catch (UnsupportedCommOperationException ex) {
            System.out.println(ex.getMessage());
            System.exit(0);
        }
    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Port Open/Close Functions">
    public void close() {
        serialPort.close();
    }

    private void OpenPort(String portName) {
        boolean portFound = false;
        CommPortIdentifier commPortIdentifier = null;
        Enumeration portList = null;

        portList = CommPortIdentifier.getPortIdentifiers();

        while (portList.hasMoreElements()) {
            commPortIdentifier = (CommPortIdentifier) portList.nextElement();
            if (commPortIdentifier.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                if (commPortIdentifier.getName().equals(portName)) {
                    System.out.println("Found port: " + portName);
                    portFound = true;
                    break;
                }
            }
        }
        if (!portFound) {
            System.out.println("port " + portName + " not found.");
            System.exit(0);
        }
        System.out.println("Opening port: " + portName);
        try {
            CommPortIdentifier.getPortIdentifier(portName);
            serialPort = (SerialPort) commPortIdentifier.open("Micrelec", 2000);
        } catch (Exception ex) {
            System.out.println("Error opening port: " + ex.getMessage());
            System.exit(0);
        }
        try {
            inputStream = serialPort.getInputStream();
            outputStream = serialPort.getOutputStream();
            serialPort.enableReceiveTimeout(RECEIVE_TIMEOUT);
        } catch (Exception ex) {
            System.out.println("Error getting Streams: " + ex.getMessage());
            System.exit(0);
        }

                try {
            serialPort.enableReceiveTimeout(RECEIVE_TIMEOUT);
        } catch (UnsupportedCommOperationException ex) {
            System.out.println("Error enabling receive timeout: " + ex.getMessage());
            String readLine = System.console().readLine();
        }

    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Char Send/Receive Functions">
    public boolean txChar(byte charToSend) {
        try {
            outputStream.write(charToSend);
        } catch (Exception ex) {
            System.out.println("Error writing char: " + ex.getMessage());
            return false;
        }
        return true;

    }

    public byte rxChar() {
        InputStream input = null;
        byte receivedChar = 0x00;
        try {
            input = serialPort.getInputStream();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return 0x00;
        }
        try {
            receivedChar = (byte) input.read();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return 0x00;
        }
        if (receivedChar == -1) {
            receivedChar = 0x00;
        }
        return receivedChar;
    }// </editor-fold>
}
