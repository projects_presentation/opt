package MicrelecPackage;

import MainPackage.MainWindow;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MicrelecChannel {

    public enum state {

        FREE,
        WAITING_TO_BE_RESERVED,
        RESERVED,
        WAITING_TO_SELECT_DISPENSER,
        DISPENSER_MESSAGE_SENT,
        DISPENSER_SELECTED,
        SETTING_MAXIMUN_CREDIT_AMOUNT,
        WAITING_NOOZLE_OUT_MESSAGE,
        WAITING_TO_START_DISPENSING,
        STARTED_DISPENSING,
        WAITING_TO_COMPLETE_TRANSACTION,
        TRANSACTION_COMPLETED,
        ERROR,
        WAITING_TO_BE_RELEASED,
        RELEASED,
        WAITING_TO_SEND_FILLING
    }
    private String lastReceivedMessage = "";
    private int channelNumber = 0;
    private state channelState;
    private int pumpNumber = 0;
    private int transactionNumber = 0;
    private int maxCreditAmount = 0;
    private MainWindow mainWindow;

    public MicrelecChannel(int channelNumber, MainWindow mainWindow) {
        this.channelNumber = channelNumber;
        this.mainWindow = mainWindow;
        setChannelState(channelState.FREE);
    }

    public int getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(int selectedDispenser) {
        this.pumpNumber = selectedDispenser;
    }

    public int getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(int transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public void setMaxCreditAmount(int creditAmount) {
        maxCreditAmount = creditAmount;
    }

    public int getMaxCreditAmount() {
        return maxCreditAmount;
    }

    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    public int getChannelNumber() {
        return channelNumber;
    }

    public state getChannelState() {
        return channelState;
    }

    public boolean setChannelState(state state) {
        channelState = state;
        mainWindow.updateChannelState(state.toString(), (channelNumber + 1));
        return true;
    }

    public String getLastReceivedMessage() {
        return lastReceivedMessage;
    }

    public void setLastReceivedMessage(String lastReceivedMessage) {
        this.lastReceivedMessage = lastReceivedMessage;
    }

    public byte[] GetReserveChannelMessage() {
        byte[] formatedMessage = new byte[10];
        int index = 0;
        String messageToSend = "Q0";
        formatedMessage[index++] = SerialPortControl.STX;
        for (int i = 0; i < messageToSend.length(); ++i) {
            formatedMessage[(index) + i] = (byte) messageToSend.charAt(i);
        }
        index += messageToSend.length();
        formatedMessage[index++] = SerialPortControl.ETX;
        formatedMessage[index++] = calculateLrc(formatedMessage);

        return formatedMessage;
    }

    public byte[] GetReservePumpMessage(int pumpNumber) {
        byte[] formatedMessage = new byte[10];
        int index = 0;

        NumberFormat nf = new DecimalFormat("00");
        String messageToSend = "P" + nf.format(pumpNumber);
        formatedMessage[index++] = SerialPortControl.STX;
        for (int i = 0; i < messageToSend.length(); ++i) {
            formatedMessage[(index) + i] = (byte) messageToSend.charAt(i);
        }
        index += messageToSend.length();
        formatedMessage[index++] = SerialPortControl.ETX;
        formatedMessage[index++] = calculateLrc(formatedMessage);
        return formatedMessage;
    }

    public byte[] GetMaxCreditAmountMessage() {
        byte[] formatedMessage = new byte[100];
        int index = 0;

        NumberFormat nf1 = new DecimalFormat("00");
        NumberFormat nf2 = new DecimalFormat("000000");
        String messageToSend = "C" + nf1.format(pumpNumber) + nf2.format(maxCreditAmount) + "00000";
        formatedMessage[index++] = SerialPortControl.STX;
        for (int i = 0; i < messageToSend.length(); ++i) {
            formatedMessage[(index) + i] = (byte) messageToSend.charAt(i);
        }
        index += messageToSend.length();
        formatedMessage[index++] = SerialPortControl.ETX;
        formatedMessage[index++] = calculateLrc(formatedMessage);
        return formatedMessage;
    }

    public byte[] GetStartDispensingMessage(int dispenser, int transaction) {
        byte[] formatedMessage = new byte[100];
        int index = 0;

        NumberFormat nf1 = new DecimalFormat("00");
        NumberFormat nf2 = new DecimalFormat("0000");
        String messageToSend = "S" + nf1.format(dispenser) + nf2.format(transaction);
        formatedMessage[index++] = SerialPortControl.STX;
        for (int i = 0; i < messageToSend.length(); ++i) {
            formatedMessage[(index) + i] = (byte) messageToSend.charAt(i);
        }
        index += messageToSend.length();
        formatedMessage[index++] = SerialPortControl.ETX;
        formatedMessage[index++] = calculateLrc(formatedMessage);
        return formatedMessage;
    }

    public byte[] GetEndOfTransactionMessage(int transaction) {
        byte[] formatedMessage = new byte[100];
        int index = 0;
        NumberFormat nf = new DecimalFormat("0000");
        String messageToSend = "A" + nf.format(transaction);
        formatedMessage[index++] = SerialPortControl.STX;
        for (int i = 0; i < messageToSend.length(); ++i) {
            formatedMessage[(index) + i] = (byte) messageToSend.charAt(i);
        }
        index += messageToSend.length();
        formatedMessage[index++] = SerialPortControl.ETX;
        formatedMessage[index++] = calculateLrc(formatedMessage);
        return formatedMessage;
    }

    public byte[] getReleaseChannelMessage() {
        byte[] formatedMessage = new byte[100];
        int index = 0;
        NumberFormat nf = new DecimalFormat("00");
        String messageToSend = "R" + nf.format(channelNumber);
        formatedMessage[index++] = SerialPortControl.STX;
        for (int i = 0; i < messageToSend.length(); ++i) {
            formatedMessage[(index) + i] = (byte) messageToSend.charAt(i);
        }
        index += messageToSend.length();
        formatedMessage[index++] = SerialPortControl.ETX;
        formatedMessage[index++] = calculateLrc(formatedMessage);
        return formatedMessage;
    }

    public byte[] getSendFillingMessage(String message) {
        byte[] formatedMessage = new byte[500];
        int index = 0;
        formatedMessage[index++] = SerialPortControl.STX;
        for (int i = 0; i < message.length(); ++i) {
            formatedMessage[(index) + i] = (byte) message.charAt(i);
        }
        index += message.length();
        formatedMessage[index++] = SerialPortControl.ETX;
        formatedMessage[index++] = calculateLrc(formatedMessage);
        return formatedMessage;
    }

    public byte calculateLrc(byte message[]) {
        int i = 1;
        byte lrc = 0x00;
        while (message[i] != SerialPortControl.ETX) {
            lrc ^= message[i];
            ++i;
        }
        lrc ^= message[i];
        return lrc;
    }
}
