package MicrelecPackage;

import DatabasePackage.DatabaseInsertions;
import FileLogger.FileLogger;
import MainPackage.MainWindow;
import MicrelecPackage.MicrelecChannel.state;
import RegistryManager.RegistryManager;
import TPAPackage.SecurityModule;
import java.util.ArrayList;
import javax.comm.SerialPort;

public class Micrelec extends Thread {

    public SerialPortControl serialPort = null;
    private boolean communicating = false;
    private MicrelecChannel actualMicrelecChannel;
    private DatabaseInsertions databaseInsertions;
    private final int MAX_MICRELEC_CHANNELS = 8;
    private ArrayList<MicrelecChannel> listofChannels;
    private ArrayList<SecurityModule> listofSecurityModules;
    private FileLogger logger;
    private MainWindow mainWindow;
    private RegistryManager registryManager;
    private final String REGISTRY_ROOT = "SOFTWARE\\GWorks\\OPT\\";

    public Micrelec(String serialPortName, ArrayList<SecurityModule> listofSecurityModules, MainWindow mainWindow) {
        this.logger = new FileLogger("Micrelec", REGISTRY_ROOT);
        this.mainWindow = mainWindow;
        serialPort = new SerialPortControl(serialPortName, 9600, SerialPort.STOPBITS_1, SerialPort.PARITY_ODD, SerialPort.DATABITS_7);
        registryManager = new RegistryManager(REGISTRY_ROOT);
        listofChannels = new ArrayList<MicrelecChannel>(MAX_MICRELEC_CHANNELS);
        listofSecurityModules = new ArrayList<SecurityModule>(Integer.parseInt(registryManager.readValue("Settings", "Number of sides", false)));
        this.listofSecurityModules = listofSecurityModules;
        startMicrelecChannels();
        databaseInsertions = new DatabaseInsertions(logger);
        this.start();
    }

    public SecurityModule getSecurityModuleToPerfomPayment(int pumpNumber) {
        for (int i = 0; i < listofSecurityModules.size(); ++i) {
            if (listofSecurityModules.get(i).getPumpNumber() == pumpNumber) {
                return listofSecurityModules.get(i);
            }
        }
        return null;
    }

    public void setActualMicrelecChannel(int channelNumber) {
        try {
            actualMicrelecChannel = listofChannels.get(channelNumber);
        } catch (Exception ex) {
            logger.logException(ex, logger.WARNING);
        }
    }

    public boolean isCommunicating() {
        return communicating;
    }

    private void setCommunication(boolean communication) {
        communicating = communication;
        if (!communicating) {
            mainWindow.updateMicrelecState("Micrelec não comunica!");
            logger.logString("Micrelec Stoped communicating...", logger.WARNING);
            return;
        } else {
            mainWindow.updateMicrelecState("A comunicar...");

        }
    }

    public void startMicrelecChannels() {
        MicrelecChannel newChannel;
        for (int i = 0; i < MAX_MICRELEC_CHANNELS; ++i) {
            newChannel = new MicrelecChannel(i, mainWindow);
            listofChannels.add(newChannel);
            mainWindow.updateChannelState("FREE", i);
        }
    }

    public void run() {
        int maxCommunicatingTries = 0;
        String receivedMessage = "";
        String fillingForSiac = "";
        int sendFillingTry = 0;
        state actualChannelState = MicrelecChannel.state.FREE;
        mainWindow.updateMicrelecState("A iniciar comunicação...");
        while (true) {
            maxCommunicatingTries = 0;
            while (true) {
                while (!waitEOTENQ()) {
                    if (maxCommunicatingTries++ == 300) {
                        setCommunication(false);
                    }
                }
                maxCommunicatingTries = 0;
                setCommunication(true);
                while (true) {
                    while ((receivedMessage = waitPSEOT()).equals("")) {
                        if (maxCommunicatingTries++ == 300) {
                            setCommunication(false);
                        }
                    }
                    maxCommunicatingTries = 0;
                    while (!getChannel()) {
                        if (maxCommunicatingTries++ == 300) {
                            setCommunication(false);
                        }
                    }
                    maxCommunicatingTries = 0;
                    actualChannelState = actualMicrelecChannel.getChannelState();
                    if (receivedMessage.equals("P")) {
                        if (actualChannelState == MicrelecChannel.state.WAITING_TO_BE_RESERVED) {
                            sendMessageToMicrelec(actualMicrelecChannel.GetReserveChannelMessage());
                            break;
                        }
                        if (actualChannelState == MicrelecChannel.state.WAITING_TO_BE_RELEASED) {
                            sendMessageToMicrelec(actualMicrelecChannel.getReleaseChannelMessage());
                            actualMicrelecChannel.setChannelState(MicrelecChannel.state.FREE);
                            break;
                        }
                        if (actualChannelState == MicrelecChannel.state.WAITING_TO_SELECT_DISPENSER) {
                            sendMessageToMicrelec(actualMicrelecChannel.GetReservePumpMessage(actualMicrelecChannel.getPumpNumber()));
                            actualMicrelecChannel.setChannelState(MicrelecChannel.state.DISPENSER_MESSAGE_SENT);
                            break;
                        }
                        if (actualChannelState == MicrelecChannel.state.SETTING_MAXIMUN_CREDIT_AMOUNT) {
                            sendMessageToMicrelec(actualMicrelecChannel.GetMaxCreditAmountMessage());
                            actualMicrelecChannel.setChannelState(MicrelecChannel.state.WAITING_NOOZLE_OUT_MESSAGE);
                            break;
                        }
                        if (actualChannelState == MicrelecChannel.state.WAITING_TO_START_DISPENSING) {
                            sendMessageToMicrelec(actualMicrelecChannel.GetStartDispensingMessage(actualMicrelecChannel.getPumpNumber(), actualMicrelecChannel.getTransactionNumber()));
                            break;
                        }
                        if (actualChannelState == MicrelecChannel.state.TRANSACTION_COMPLETED) {

                            System.out.println("Transaction completed!!!");
                            sendMessageToMicrelec(actualMicrelecChannel.GetEndOfTransactionMessage(actualMicrelecChannel.getTransactionNumber()));
                            actualMicrelecChannel.setChannelState(MicrelecChannel.state.FREE);
                            break;

                        }

                        if (actualMicrelecChannel.getChannelNumber() == 7) {

                            if (sendFillingTry++ == 10) {
                                sendFillingTry = 0;
                                System.out.println("Checking for fillings to send to SIAC!");
                                fillingForSiac = databaseInsertions.getFillingForSiac();
                                if (!fillingForSiac.equals("")) {
                                    //if (false) {
                                    System.out.println("Sending filling: " + fillingForSiac);
                                    mainWindow.updateMicrelecState("A enviar abastecimento: " + fillingForSiac);
                                    logger.logString("Sending filling to Siac:" + fillingForSiac, logger.INFO);
                                    sendMessageToMicrelec(actualMicrelecChannel.getSendFillingMessage(fillingForSiac));
                                } else {
                                    System.out.println("No filling to send to siac!");
                                }
                            }
                        }
                        sendEOT();
                        break;
                    } else if (receivedMessage.equals("S")) {
                        receivedMessage = receiveMessageFromMicrelec();
                        if (actualChannelState == MicrelecChannel.state.FREE) {
                            if (receivedMessage.equals("T")) {
                                actualMicrelecChannel.setChannelState(MicrelecChannel.state.TRANSACTION_COMPLETED);
                                actualMicrelecChannel.setTransactionNumber(insertPendingTransaction(actualMicrelecChannel.getLastReceivedMessage()));
                                logger.logString("New pending transaction", logger.INFO);
                            }
                        }
                        if (actualChannelState == MicrelecChannel.state.WAITING_TO_BE_RESERVED) {
                            if (receivedMessage.equals("A")) {
                                actualMicrelecChannel.setChannelState(MicrelecChannel.state.WAITING_TO_SELECT_DISPENSER);
                            }
                            if (receivedMessage.equals("N")) {
                                actualMicrelecChannel.setChannelState(MicrelecChannel.state.ERROR);
                            }
                        }
                        if (actualChannelState == MicrelecChannel.state.DISPENSER_MESSAGE_SENT) {
                            if (receivedMessage.equals("A")) {
                                actualMicrelecChannel.setChannelState(MicrelecChannel.state.DISPENSER_SELECTED);
                            }
                            if (receivedMessage.equals("N")) {
                                actualMicrelecChannel.setChannelState(MicrelecChannel.state.ERROR);
                            }
                        }
                        if (actualChannelState == MicrelecChannel.state.WAITING_NOOZLE_OUT_MESSAGE) {
                            if (receivedMessage.equals("P")) {
                                actualMicrelecChannel.setChannelState(MicrelecChannel.state.WAITING_TO_START_DISPENSING);
                            } else {
                                actualMicrelecChannel.setChannelState(MicrelecChannel.state.ERROR);
                            }
                        }
                        if (actualChannelState == MicrelecChannel.state.WAITING_TO_START_DISPENSING) {
                            if (receivedMessage.equals("A")) {
                                actualMicrelecChannel.setChannelState(MicrelecChannel.state.STARTED_DISPENSING);
                            }
                        }
                        if (actualChannelState == MicrelecChannel.state.STARTED_DISPENSING) {
                            if (receivedMessage.equals("T")) {
                                actualMicrelecChannel.setChannelState(MicrelecChannel.state.TRANSACTION_COMPLETED);
                            }
                        }
                        if (receivedMessage.equals("m")) {
                            System.out.println("Message received from Siac: " + actualMicrelecChannel.getLastReceivedMessage());
                            mainWindow.updateMicrelecState("Mensagem recebida do SIAC: " + actualMicrelecChannel.getLastReceivedMessage());
                            logger.logString("Message received from Siac:" + actualMicrelecChannel.getLastReceivedMessage(), logger.INFO);
                            if (actualMicrelecChannel.getLastReceivedMessage().endsWith("00")) {
                                databaseInsertions.updateFillingValueByCommandNumber("STATE", actualMicrelecChannel.getLastReceivedMessage().substring(5, 7), "SENT");
                            } else {
                                System.out.println("Error! The message was sent but not received in SIAC");
                                logger.logString("Error! The message was sent but not received in SIAC", logger.INFO);
                            }

                        }

                        break;
                    }
                }

                break;
            }
        }
    }

    private int insertPendingTransaction(String message) {

        String dispenser = "";
        String value = "";
        String volume = "";
        String pricePerUnit = "";
        String productCode = "";
        String transactionID = "";
        String transactionNumber = "";


        dispenser = message.substring(1, 3);
        value = message.substring(3, 9);
        volume = message.substring(9, 15);
        pricePerUnit = message.substring(15, 20);
        productCode = message.substring(20, 22);
        transactionNumber = message.substring(24, 28);

        logger.logString("New pending transaction! Pump: " + dispenser + " Transaction Number: " + transactionNumber, value);

        transactionID = databaseInsertions.getTransactionIDByTransactionNumber(transactionNumber);
        databaseInsertions.insertPendingTransaction(value, transactionID, dispenser, transactionNumber);
        databaseInsertions.updateFillingAfterAborted(transactionNumber, value, volume, productCode, pricePerUnit);

        return Integer.parseInt(transactionNumber);

    }

    public synchronized MicrelecChannel getNextAvaiableChannel(int sideNumber) {
        switch (sideNumber) {
            case 1:
                if (listofChannels.get(0).getChannelState() == MicrelecChannel.state.FREE) {
                    listofChannels.get(0).setChannelState(MicrelecChannel.state.WAITING_TO_BE_RESERVED);
                    return listofChannels.get(0);
                }
            case 2:
                if (listofChannels.get(1).getChannelState() == MicrelecChannel.state.FREE) {
                    listofChannels.get(1).setChannelState(MicrelecChannel.state.WAITING_TO_BE_RESERVED);
                    return listofChannels.get(1);
                }
            default:
                System.out.println("Error no channel for side: " + sideNumber);
                logger.logString("Error no channel for side: " + sideNumber, logger.WARNING);
                return null;
        }


    }

    private boolean waitEOTENQ() {
        byte receivedChar = 0x00;
        receivedChar = serialPort.rxChar();
        if (receivedChar == SerialPortControl.EOT) {
            return true;
        } else if (receivedChar == SerialPortControl.ENQ) {
            return true;
        } else {
            return false;
        }

    }

    private String waitPSEOT() {
        byte receivedChar = 0x00;
        receivedChar = serialPort.rxChar();
        if (receivedChar == SerialPortControl.P) {
            return "P";
        } else if (receivedChar == SerialPortControl.S) {
            return "S";
        } else if (receivedChar == SerialPortControl.m) {
            return "m";
        } else {
            return "";
        }
    }

    private boolean getChannel() {
        byte receivedChar = 0x00;
        int channelNumber = 0;
        receivedChar = serialPort.rxChar();
        channelNumber = (receivedChar - 0x30);
        if (channelNumber >= 8 && channelNumber < 0) {
            return false;
        } else {
            setActualMicrelecChannel(channelNumber);
            mainWindow.updateMicrelecChannel(channelNumber);
            return true;
        }
    }

    private void sendEOT() {
        serialPort.txChar(SerialPortControl.EOT);
    }

    private void sendAck() {
        serialPort.txChar(SerialPortControl.ACK);
    }

    private boolean getAck() {
        if (serialPort.rxChar() == SerialPortControl.ACK) {
            return true;
        } else {
            return false;
        }
    }

    public void sendMessageToMicrelec(byte[] messageToSend) {
        int index = 0;
        mainWindow.updateMicrelecState("A enviar mensagem: " + new String(messageToSend));
        while (messageToSend[index] != SerialPortControl.ETX) {
            serialPort.txChar(messageToSend[index++]);
        }
        serialPort.txChar(messageToSend[index++]);
        serialPort.txChar(messageToSend[index]);
        index = 0;
        while (!getAck()) {
            if (index++ == 20) {
                return;
            }
        }
        sendEOT();
    }

    private void sendNack() {
        serialPort.txChar(SerialPortControl.NAK);
    }

    public String receiveMessageFromMicrelec() {
        byte[] receivedMessage = new byte[10000];
        int index = 0;
        byte lrc = 0x00;
        byte receivedChar = 0x00;
        int STX = 0;
        int ETX = 0;
        sendAck();
        while (true) {
            receivedChar = serialPort.rxChar();
            receivedMessage[index] = receivedChar;
            if (receivedMessage[index] == SerialPortControl.ETX) {
                ETX = index;
                break;
            }
            if (receivedMessage[index] == SerialPortControl.STX) {
                STX = index + 1;
            }
            if (receivedChar != 0x00) {
                index++;
            }
            if (index > 5000) {
                break;
            }
            try {
                Thread.sleep(2);
            } catch (InterruptedException ex) {
            }
        }
        lrc = serialPort.rxChar();

        if (lrc == actualMicrelecChannel.calculateLrc(receivedMessage)) {
            sendAck();
        } else {
            logger.logString("Error! NAK sent to micrelec for message: " + receivedMessage, logger.WARNING);

            sendNack();
            serialPort.rxChar();
            sendEOT();
            return "";
        }
        serialPort.rxChar();
        actualMicrelecChannel.setLastReceivedMessage(new String(receivedMessage, STX, ETX - STX));
        mainWindow.updateMicrelecLastReceivedMessage(actualMicrelecChannel.getLastReceivedMessage());
        sendEOT();
        if (receivedMessage[STX] == SerialPortControl.A) {
            return "A";
        }
        if (receivedMessage[STX] == SerialPortControl.N) {
            return "N";
        }
        if (receivedMessage[STX] == SerialPortControl.P) {
            return "P";
        }
        if (receivedMessage[STX] == SerialPortControl.T) {
            logger.logString("Transaction received from micrelec: " + new String(receivedMessage, STX, ETX - STX), logger.INFO);
            return "T";
        }
        if (receivedMessage[STX] == SerialPortControl.m) {
            return "m";
        } else {
            return "";
        }
    }
}
