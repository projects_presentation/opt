package TPAPackage;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;

public class SecurityModuleFunctions {

    public static final byte DLE = 0x10;
    public static final byte ACK = 0x06;
    public static final byte ENQ = 0x05;
    public static final byte STX = 0x02;
    public static final byte ETX = 0x03;
    private final String PRINTER_COMMAND_PROPRIETARY = "LP";
    private final String COMMAND_PROPRIETARY = "GS";
    private final String CARD_SERVICE_REQUEST = "01";
    private final String GET_PRINTER_STATUS = "01";
    private final String MAINTENACE_OPERATIONS = "02";
    private final String SUPERVISOR_OPERATIONS = "03";
    private final String OPEN_PERIOD = "04";
    private final String CLOSE_PERIOD = "05";
    private final String PERFORM_AUTHORIZATION = "08";
    private final String PERFORM_PAYMENT = "09";
    private final String RETURN_TO_IDLE_STATE = "11";
    private final String CHECK_PERIOD_STATE = "12";
    private final String CARD_PRESENCE = "13";
    private final String OPEN_KEYBOARD_FOR_INFO = "14";
    private final String GET_CIPHERED_TRACK = "15";
    private final String GET_MERCHANT_CARD_DATA = "16";
    private final String PRINT_MESSAGE = "20";
    private final String GET_BIN_FROM_CHIP = "23";
    private final String SET_PRIVATE_BINS_LIST = "26";
    private final String GET_PRIVATE_BINS_LIST = "27";
    private final String GET_DISCRETIONARY_DATA = "28";
    private final byte[] PRINTER_STATUS_MESSAGE = {0x1D, 0x72, 0x31};
    private final byte COMMAND_VERSION_1 = 0x01;
    private final byte COMMAND_VERSION_2 = 0x02;
    private final byte TPA_ONLINE = 0x00;
    private final byte TPA_OFFLINE = 0x01;
    private final byte REQUEST_CARD = 0x00;
    private final byte DONT_REQUEST_CARD = 0x01;
    private final byte PRINT_LOCAL = 0x01;
    private final byte FUEL_CODE = 0x00;
    private final byte LOCK_CARD = 0x00;
    private final byte DONT_LOCK_CARD = 0x01;
    private final byte CENTRAL_OPERATION = 0x01;

    public SecurityModuleFunctions() {
    }

    public byte[] getMaintenanceOperationsMessage() {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < MAINTENACE_OPERATIONS.length(); ++i) {
            message[index++] = (byte) MAINTENACE_OPERATIONS.charAt(i);
        }
        message[index++] = COMMAND_VERSION_1;

        return formatMessage(message, index);
    }

    public byte[] getOpenPeriodMessage() {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < OPEN_PERIOD.length(); ++i) {
            message[index++] = (byte) OPEN_PERIOD.charAt(i);
        }
        message[index++] = COMMAND_VERSION_1;
        message[index++] = CENTRAL_OPERATION;

        return formatMessage(message, index);
    }

    public byte[] getClosePeriodMessage() {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < CLOSE_PERIOD.length(); ++i) {
            message[index++] = (byte) CLOSE_PERIOD.charAt(i);
        }
        message[index++] = COMMAND_VERSION_1;
        message[index++] = CENTRAL_OPERATION;

        return formatMessage(message, index);
    }

    public byte[] getCheckPeriodStateMessage() {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < CHECK_PERIOD_STATE.length(); ++i) {
            message[index++] = (byte) CHECK_PERIOD_STATE.charAt(i);
        }
        message[index++] = COMMAND_VERSION_1;

        return formatMessage(message, index);
    }

    public byte[] getSupervisorOperationsMessage() {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < SUPERVISOR_OPERATIONS.length(); ++i) {
            message[index++] = (byte) SUPERVISOR_OPERATIONS.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;

        return formatMessage(message, index);
    }

    public byte[] getOpenPeriodMessage(boolean requestCard) {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < OPEN_PERIOD.length(); ++i) {
            message[index++] = (byte) OPEN_PERIOD.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;

        if (requestCard) {
            message[index++] = REQUEST_CARD;
        } else {
            message[index++] = DONT_REQUEST_CARD;
        }

        return formatMessage(message, index);
    }

    public byte[] getPerformAuthorizationMessage(int maxPayment) {
        int index = 0;
        NumberFormat nf = new DecimalFormat("00000000");
        String maxAmount = nf.format(maxPayment);
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < PERFORM_AUTHORIZATION.length(); ++i) {
            message[index++] = (byte) PERFORM_AUTHORIZATION.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;

        for (int i = 0; i < maxAmount.length(); ++i) {
            message[index++] = (byte) maxAmount.charAt(i);
        }
        return formatMessage(message, index);
    }

    public byte[] getPerformPurchaseAfterAuthorizationMessage(int value, String transactionId) {
        int index = 0;

        String extraChars = "000000000000000000";
        NumberFormat nf = new DecimalFormat("00000000");
        String valueToPay = nf.format(value);

        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < PERFORM_PAYMENT.length(); ++i) {
            message[index++] = (byte) PERFORM_PAYMENT.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;
        message[index++] = PRINT_LOCAL;

        for (int i = 0; i < valueToPay.length(); ++i) {
            message[index++] = (byte) valueToPay.charAt(i);
        }

        for (int i = 0; i < transactionId.length(); ++i) {
            message[index++] = (byte) transactionId.charAt(i);
        }

        message[index++] = FUEL_CODE;

        for (int i = 0; i < extraChars.length(); ++i) {
            message[index++] = (byte) extraChars.charAt(i);
        }

        return formatMessage(message, index);
    }

    public byte[] getCardPresenceMessage(boolean lockCard) {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < CARD_PRESENCE.length(); ++i) {
            message[index++] = (byte) CARD_PRESENCE.charAt(i);
        }

        message[index++] = COMMAND_VERSION_2;

        if (lockCard) {
            message[index++] = LOCK_CARD;
        } else {
            message[index++] = DONT_LOCK_CARD;
        }

        return formatMessage(message, index);
    }

    public byte[] getOpenKeyboardForInfoMessage(int minimunDigits, int maximumDigits, String displayMessage1, String displayMessage2) {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < OPEN_KEYBOARD_FOR_INFO.length(); ++i) {
            message[index++] = (byte) OPEN_KEYBOARD_FOR_INFO.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;

        message[index++] = (byte) minimunDigits;
        message[index++] = (byte) maximumDigits;


        message[index++] = getStringSize(displayMessage1);

        for (int i = 0; i < displayMessage1.length(); ++i) {
            message[index++] = (byte) displayMessage1.charAt(i);
        }

        message[index++] = getStringSize(displayMessage2);

        for (int i = 0; i < displayMessage2.length(); ++i) {
            message[index++] = (byte) displayMessage2.charAt(i);
        }

        return formatMessage(message, index);
    }

    public byte[] getPrintMessage(String messageToPrint) {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < PRINT_MESSAGE.length(); ++i) {
            message[index++] = (byte) PRINT_MESSAGE.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;

        message[index++] = getSizeofReceipt(messageToPrint.length())[1];
        message[index++] = getSizeofReceipt(messageToPrint.length())[0];

        for (int i = 0; i < messageToPrint.length(); ++i) {
            message[index++] = (byte) messageToPrint.charAt(i);
        }

        return formatMessage(message, index);
    }

    public byte[] getCardServiceRequestMessage(String displayMessage1, String displayMessage2) {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < CARD_SERVICE_REQUEST.length(); ++i) {
            message[index++] = (byte) CARD_SERVICE_REQUEST.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;

        message[index++] = getStringSize(displayMessage1);

        for (int i = 0; i < displayMessage1.length(); ++i) {
            message[index++] = (byte) displayMessage1.charAt(i);
        }

        message[index++] = getStringSize(displayMessage2);

        for (int i = 0; i < displayMessage2.length(); ++i) {
            message[index++] = (byte) displayMessage2.charAt(i);
        }

        return formatMessage(message, index);
    }

    public byte[] getCipheredTrackMessage(String displayMessage1, String displayMessage2, int maxAmount) {
        int index = 0;
        String maxPayment = "";
        NumberFormat nf = new DecimalFormat("00000000");
        maxPayment = nf.format(maxAmount);
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < GET_CIPHERED_TRACK.length(); ++i) {
            message[index++] = (byte) GET_CIPHERED_TRACK.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;

        for (int i = 0; i < maxPayment.length(); ++i) {
            message[index++] = (byte) maxPayment.charAt(i);
        }

        message[index++] = getStringSize(displayMessage1);

        for (int i = 0; i < displayMessage1.length(); ++i) {
            message[index++] = (byte) displayMessage1.charAt(i);
        }

        message[index++] = getStringSize(displayMessage2);

        for (int i = 0; i < displayMessage2.length(); ++i) {
            message[index++] = (byte) displayMessage2.charAt(i);
        }

        return formatMessage(message, index);
    }

    public byte[] getReturnToIdleStateMessage(boolean TPAOffline, String displayMessage1, String displayMessage2) {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }
        for (int i = 0; i < RETURN_TO_IDLE_STATE.length(); ++i) {
            message[index++] = (byte) RETURN_TO_IDLE_STATE.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;

        if (TPAOffline) {
            message[index++] = TPA_OFFLINE;
        } else {
            message[index++] = TPA_ONLINE;
        }

        message[index++] = getStringSize(displayMessage1);

        for (int i = 0; i < displayMessage1.length(); ++i) {
            message[index++] = (byte) displayMessage1.charAt(i);
        }

        message[index++] = getStringSize(displayMessage2);

        for (int i = 0; i < displayMessage2.length(); ++i) {
            message[index++] = (byte) displayMessage2.charAt(i);
        }

        return formatMessage(message, index);

    }

    public byte[] getPrinterStatusMessage() {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < PRINTER_COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) PRINTER_COMMAND_PROPRIETARY.charAt(i);
        }

        for (int i = 0; i < GET_PRINTER_STATUS.length(); ++i) {
            message[index++] = (byte) GET_PRINTER_STATUS.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;
        message[index++] = 0x01;

        message[index++] = 0x04;
        message[index++] = 0x00;

        for (int i = 0; i < 3; ++i) {
            message[index++] = (byte) PRINTER_STATUS_MESSAGE[i];
        }

        return formatMessage(message, index);
    }

        public byte[] getMerchantCardDataMessage() {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }

        for (int i = 0; i < GET_MERCHANT_CARD_DATA.length(); ++i) {
            message[index++] = (byte) GET_MERCHANT_CARD_DATA.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;
        return formatMessage(message, index);
    }

    public byte[] GetBINFromChipMessage(){
        int index = 0;
        String value = "00010000";
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }

        for (int i = 0; i < GET_BIN_FROM_CHIP.length(); ++i) {
            message[index++] = (byte) GET_BIN_FROM_CHIP.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;
        for (int i = 0; i < value.length(); ++i) {
            message[index++] = (byte) value.charAt(i);
        }

        return formatMessage(message, index);
    }

    public byte[] SetPrivateBINsListMessage(ArrayList listOfBINs) {
        int index = 0;
        Iterator BINsIterator = listOfBINs.iterator();
        String iteratedBIN = "";

        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }

        for (int i = 0; i < SET_PRIVATE_BINS_LIST.length(); ++i) {
            message[index++] = (byte) SET_PRIVATE_BINS_LIST.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;

        /*Versão da tabela de BINs*/
        message[index++] = 0x00;
        message[index++] = 0x00;

        message[index++] = (byte) listOfBINs.size();

        while(BINsIterator.hasNext()){
            iteratedBIN = (String) BINsIterator.next();
            message[index++] = (byte) iteratedBIN.length();
            for (int j = 0; j < iteratedBIN.length(); ++j) {
                    message[index++] = (byte) iteratedBIN.charAt(j);
                }
        }
        return formatMessage(message, index);
    }

    public byte[] GetPrivateBINsListMessage() {
        int index = 0;
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }

        for (int i = 0; i < GET_PRIVATE_BINS_LIST.length(); ++i) {
            message[index++] = (byte) GET_PRIVATE_BINS_LIST.charAt(i);
        }
        message[index++] = COMMAND_VERSION_1;
        message[index++] = 0x0F;
        return formatMessage(message, index);
    }
    
        public byte[] GetDiscretionaryDataMessage(){
        int index = 0;
        String value = "00010000";
        byte[] message = new byte[10000];
        for (int i = 0; i < COMMAND_PROPRIETARY.length(); ++i) {
            message[index++] = (byte) COMMAND_PROPRIETARY.charAt(i);
        }

        for (int i = 0; i < GET_DISCRETIONARY_DATA.length(); ++i) {
            message[index++] = (byte) GET_DISCRETIONARY_DATA.charAt(i);
        }

        message[index++] = COMMAND_VERSION_1;
        for (int i = 0; i < value.length(); ++i) {
            message[index++] = (byte) value.charAt(i);
        }

        return formatMessage(message, index);
    }

    private byte getStringSize(String message) {
        return (byte) message.length();
    }

    private byte[] getSizeofReceipt(int lenght) {
        byte[] bytes = new byte[2];
        for (int i = 0; i < 2; ++i) {
            int offset = (1 - i) * 8;
            bytes[i] = (byte) ((lenght & (0xff << offset)) >>> offset);
        }
        return bytes;
    }

    public byte[] formatMessage(byte[] messageToSend, int size) {
        byte[] formatedMessage = new byte[10000];
        int index = 0;
        formatedMessage[index++] = DLE;
        formatedMessage[index++] = STX;

        for (int i = 0; i < size; ++i) {
            formatedMessage[(index) + i] = messageToSend[i];
        }
        index += size;
        formatedMessage[index++] = DLE;
        formatedMessage[index++] = ETX;
        formatedMessage[index++] = calculateLrc(formatedMessage);
        return formatedMessage;

    }

    public byte calculateLrc(byte message[]) {
        int i = 2;
        byte lrc = 0x00;
        while (!(message[i] == DLE && message[i + 1] == ETX)) {
            lrc ^= message[i];
            ++i;
        }
        lrc ^= message[i];
        lrc ^= message[i + 1];
        return lrc;
    }
}
