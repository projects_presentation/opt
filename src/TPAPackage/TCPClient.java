package TPAPackage;

import FileLogger.FileLogger;
import MainPackage.MainWindow;
import java.io.*;
import java.net.*;

public class TCPClient {

    private InputStream inputStream = null;
    public OutputStream outputStream = null;
    private Socket tcpSocket = null;
    private String host;
    private int port;
    private MainWindow mainWindow;
    private int sideNumber = 0;
    private FileLogger logger;
    private final String REGISTRY_ROOT = "SOFTWARE\\GWorks\\OPT\\";

    public TCPClient(String host, int port, MainWindow mainWindow, int sideNumber) {
        this.host = host;
        this.port = port;
        this.mainWindow = mainWindow;
        this.sideNumber = sideNumber;
        logger = new FileLogger("OPT", REGISTRY_ROOT);
    }

    public boolean OpenSocket() {
        SocketAddress socketAddress = new InetSocketAddress(host, port);
        try {
            tcpSocket = new Socket();
            tcpSocket.setSoTimeout(60000);
            tcpSocket.connect(socketAddress, 5000);
            Thread.sleep(10);
            outputStream = tcpSocket.getOutputStream();
            inputStream = tcpSocket.getInputStream();

        } catch (Exception ex) {
            System.out.println("Error opening socket: " + ex.getMessage());
            mainWindow.updateSideState("Erro a comunicar com o IP: " + host, sideNumber);
            CloseSocket();
            return false;
        }
        if(mainWindow != null) mainWindow.updateSideState("Em comunicação com o IP: " + host, sideNumber);
        return true;
    }

    public void CloseSocket() {
        try {
            tcpSocket.close();
        } catch (IOException ex) {
            System.out.println("Error closing socket: " + ex.getMessage());
            logger.logException(ex, logger.WARNING);
            mainWindow.updateSideState("Erro a fechar o socket com o IP: " + host, sideNumber);
        }
    }

    public InputStream GetInputStream() {
        return inputStream;
    }

    public OutputStream GetOutputStream() {
        return outputStream;
    }
}

