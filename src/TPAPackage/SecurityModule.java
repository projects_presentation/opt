package TPAPackage;

import DatabasePackage.DatabaseInsertions;
import FileLogger.FileLogger;
import FillingPackage.Filling;
import MainPackage.MainWindow;
import java.io.IOException;
import java.io.InputStream;
import MicrelecPackage.Micrelec;
import RegistryManager.RegistryManager;
import VCPackage.VCJumbo;

public class SecurityModule extends Thread {

    public Boolean setSupervisorCard = false;
    public Boolean setMaintenanceCard = false;
    private String lastReceivedBIN = "";
    private String IP;
    private int port;
    public TCPClient tcpclient;
    private String state = "";
    private final int MAX_ARRAY_SIZE = 10000;
    public String lastReceivedMessage = "";
    private String lastReceivedCard = "";
    private boolean hasCard = false;
    private final int DELAY_BETWEEN_MESSAGES = 100;
    public Micrelec micrelec;
    private RegistryManager registryManager;
    private RegistryManager safety;
    private int sideNumber;
    private int cardType = 0; /*
     * Card type (1 - Magnético 2 - Chip)
     */

    private final int MAGNETIC = 1;
    private final int CHIP = 2;
    private Filling filling;
    private int printerStatus = 0;
    private FileLogger logger;
    private int pumpNumber = 0;
    private String TPANumber = "";
    private DatabaseInsertions databaseInsertions;
    private MainWindow mainWindow;
    private final String REGISTRY_ROOT = "SOFTWARE\\GWorks\\OPT\\";
    private final String SAFETY_REGISTRY_ROOT = "SOFTWARE\\Safety\\";
    public VCJumbo vCJumbo = null;

    public SecurityModule(Micrelec micrelec, int sideNumber, MainWindow mainWindow, VCJumbo vCJumbo) {

        String receipt = "";
        this.micrelec = micrelec;
        this.sideNumber = sideNumber;
        this.mainWindow = mainWindow;
        this.vCJumbo = vCJumbo;

        registryManager = new RegistryManager(REGISTRY_ROOT);
        safety = new RegistryManager(SAFETY_REGISTRY_ROOT);

        IP = registryManager.readValue("TPA" + sideNumber, "IP", false);
        port = Integer.parseInt(registryManager.readValue("TPA" + sideNumber, "Port", false));
        pumpNumber = Integer.parseInt(registryManager.readValue("TPA" + sideNumber, "Pump Number", false));
        TPANumber = registryManager.readValue("TPA" + sideNumber, "TPA Number", false);

        logger = new FileLogger("Modulo de seguranca " + sideNumber, REGISTRY_ROOT);
        databaseInsertions = new DatabaseInsertions(logger);
        tcpclient = new TCPClient(IP, port, mainWindow, sideNumber);

        if (mainWindow != null) {
            mainWindow.updateSideState("A iniciar...", sideNumber);
            this.start();
        }


    }

    public String getLastReceivedCard() {
        return lastReceivedCard;
    }

    public String getTPANumber() {
        return TPANumber;
    }

    public int getSideNumber() {
        return sideNumber;
    }

    public Micrelec getMicrelec() {
        return micrelec;
    }

    public int getPumpNumber() {
        return pumpNumber;
    }

    @Override
    public void run() {
        SetPrivateBINsList();
        while (true) {
            checkCardPresence(false);
            if (micrelec != null) {
                handleCardEntry(micrelec.isCommunicating() && isWorking() && periodIsOpened());
            }
        }
    }

    private void SetPrivateBINsList() {
        if (vCJumbo.getPromotionsList() == null) {
            return;
        }
        if (vCJumbo.getPromotionsList().getListOfBINs() == null) {
            return;
        }
        while (!sendMessage(new SecurityModuleFunctions().SetPrivateBINsListMessage(vCJumbo.getPromotionsList().getListOfBINs()))) {
            delayMs(10);
        }
        if (receiveMessage()) {
        }
    }

    private void handleCardEntry(Boolean working) {

        if (cardServiceRequest(working, false)) {
            getTrack();
            if (setSupervisorCard) {
                setSupervisorCard = false;
                System.out.println("Setting supervisor card to: " + lastReceivedCard);
                registryManager.setValue("Cards", "SUPERVISOR_CARD_" + sideNumber, lastReceivedCard, false);
            }
            if (setMaintenanceCard) {
                setMaintenanceCard = false;
                System.out.println("Setting maintenance card to: " + lastReceivedCard);
                registryManager.setValue("Cards", "MAINTENANCE_CARD_" + sideNumber, lastReceivedCard, false);
            }

            if (lastReceivedCard.equals(registryManager.readValue("Cards", "MAINTENANCE_CARD_" + sideNumber, false))) {
                logger.logString("Maintenance Operations", logger.INFO);
                System.out.println("Maintenance Operations");
                maintenaceOperations();
            } else if (lastReceivedCard.equals(registryManager.readValue("Cards", "SUPERVISOR_CARD_" + sideNumber, false))) {
                logger.logString("Supervisor Operations", logger.INFO);
                System.out.println("Supervisor Operations");
                getPrinterStatus();
                supervisorOperations();
            } else if (lastReceivedCard.equals(safety.readValue("Safety", "SafetyCard", false))) {
                //enableSecurity();
            } else {
                if (working) {
                    mainWindow.updateSideState("A iniciar um novo abastecimento", sideNumber);
                    logger.logString("Starting a new Filling for side: " + sideNumber + " and card: " + lastReceivedCard, logger.INFO);
                    startNewFilling();
                }
            }
        } else {
            if (this.state.equals("50")) {
                if (cardServiceRequest(true, true)) {
                    getTrack();
                    reprintReceipt();
                }
            }
        }
    }

    private String getHex(byte[] card) {
        String aux = "";
        int b;

        for (int i = 0; i < card.length; ++i) {
            b = card[i];
            aux += Integer.toHexString(b & 0xFF).toUpperCase();
        }
        return aux;

    }

    public boolean getTrack() {

        String message1, message2;

        message1 = registryManager.readValue("MessagesToTPA", "WAIT_PT", false);
        message2 = registryManager.readValue("MessagesToTPA", "WAIT_EN", false);

        while (!sendMessage(new SecurityModuleFunctions().getCipheredTrackMessage(message1, message2, 9900))) {
            delayMs(10);
        }
        if (receiveMessage()) {
            System.out.println("Message received for card: " + getLastReceivedMessage());
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                try {
                    lastReceivedCard = getHex(lastReceivedMessage.substring(6, lastReceivedMessage.length()).getBytes());
                } catch (Exception e) {
                    System.out.println("Error reading card: " + e.getMessage());
                    return false;
                }
                System.out.println("Last received card: *" + lastReceivedCard + "*");
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private void hasPendingTransactions() {
        String ID = "";
        String value = "";
        String transactionID = "";
        String transactionNumber = "";
        ID = databaseInsertions.getIDOfPendingTransaction(pumpNumber);
        if (!ID.equals("")) {
            value = databaseInsertions.getValueByID(ID);
            transactionID = databaseInsertions.getTransactionIDByID(ID);
            transactionNumber = databaseInsertions.getTransactionNumberByTransactionID(transactionID);
            System.out.println("Pending transaction: " + transactionID + " value: " + value + " Transaction Number: " + transactionNumber);
            logger.logString("Pending transaction: " + transactionID + " value: " + value + " Transaction Number: " + transactionNumber, logger.INFO);
            if (performPaymentAfterAuthorizationWithoutReceipt(Integer.parseInt(value), transactionID, pumpNumber, Integer.parseInt(transactionNumber))) {
                databaseInsertions.updatePendingTransactionByID(ID);
                databaseInsertions.updateFillingValueByTransactionID("STATE", transactionNumber, "READY_TO_BE_SEND");
                databaseInsertions.insertReceipt(lastReceivedCard, transactionID);
            }

        } else {
            System.out.println("No pending transactions for pumpNumber: " + pumpNumber);
        }

    }

    private Boolean isWorking() {
        System.out.println(safety.readValue("Safety", "Working", false).equals("1"));
        return safety.readValue("Safety", "Working", false).equals("1");
    }

    private void reprintReceipt() {

        String receiptToReprint = "";
        receiptToReprint = databaseInsertions.retreiveReceipt(lastReceivedCard);
        if (!receiptToReprint.equals("")) {
            printReceipt("\r\n\r\n*** DUPLICADO ***\n" + receiptToReprint, true, false, false);
            if (openKeyboardForReceipt("O RECIBO FOI IMPRESSO?", "RECEIPT WAS PRINTED?")) {
                databaseInsertions.removeLastReceipt(lastReceivedCard);
            }
        } else {
            returnToIdleState("NÃO EXISTE RECIBO", "NO RECEIPT");
            delayMs(3000);
        }
    }

    private void startNewFilling() {
        pumpNumber = Integer.parseInt(registryManager.readValue("TPA" + sideNumber, "Pump Number", false));
        if (lastReceivedBIN.equals("")) {
            lastReceivedBIN = "00000000000000000000";
        }
        filling = new Filling(this, pumpNumber, mainWindow, lastReceivedBIN);
        lastReceivedBIN = "";
        filling.start();
        while (true) {
            if (filling.getFillingState().equals(filling.ABORTED)) {
                break;
            }
            if (filling.getFillingState().equals(filling.FINISHED)) {
                break;
            }
            delayMs(1);
        }
        System.out.println("Exited while...");
    }

    private void delayMs(int delayValue) {
        try {
            Thread.sleep(delayValue);
        } catch (InterruptedException ex) {
            logger.logException(ex, logger.WARNING);
        }

    }

    public void getPrinterStatus() {
        /*
         * 0 - Impressora OK 1 - Impressora sem papel 2 - Impressora com pouco
         * papel
         */
        while (!sendMessage(new SecurityModuleFunctions().getPrinterStatusMessage())) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                try {
                    if (lastReceivedMessage.charAt(8) == 0x00) {
                        printerStatus = 0;
                    }
                    if (lastReceivedMessage.charAt(8) == 0x03) {
                        printerStatus = 2;
                    }
                } catch (Exception ex) {
                    printerStatus = 3;
                }
            } else if (state.equals("89")) {
                printerStatus = 1;
            }
        } else {
            printerStatus = 2;
        }
        if (printerStatus == 2) {
            returnToIdleState(registryManager.readValue("MessagesToTPA", "PRINTER_WITH_LOW_PAPER1", false), registryManager.readValue("MessagesToTPA", "PRINTER_WITH_LOW_PAPER2", false));
        }
        if (printerStatus == 1) {
            returnToIdleState(registryManager.readValue("MessagesToTPA", "PRINTER_OUT_OF_PAPER", false), "");
        }
        delayMs(3000);
    }

    public boolean periodIsOpened() {

        while (!sendMessage(new SecurityModuleFunctions().getCheckPeriodStateMessage())) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                if (lastReceivedMessage.charAt(18) == 0x01) {
                    return true;
                } else if (lastReceivedMessage.charAt(18) == 0x00) {
                    return false;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void checkCardPresence(boolean lockCard) {/*
         * True - Bloqueia **** False - não bloqueia
         */
        cardPresence(lockCard);
        if (lockCard) {
            return;
        }
        if (hasCard) {
            returnToIdleState(registryManager.readValue("MessagesToTPA", "REMOVE_CARD_PT", false), registryManager.readValue("MessagesToTPA", "REMOVE_CARD_EN", false));
        } else {
            return;
        }

        while (true) {
            cardPresence(lockCard);
            if (!hasCard) {
                break;
            }
        }
    }

    public String getLastReceivedMessage() {
        return lastReceivedMessage;
    }

    public void setTpaState(String state) {
        this.state = state;

    }

    public boolean openPeriod() {
        while (!sendMessage(new SecurityModuleFunctions().getOpenPeriodMessage())) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean closePeriod() {
        while (!sendMessage(new SecurityModuleFunctions().getClosePeriodMessage())) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean maintenaceOperations() {

        while (!sendMessage(new SecurityModuleFunctions().getMaintenanceOperationsMessage())) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean supervisorOperations() {
        while (!sendMessage(new SecurityModuleFunctions().getSupervisorOperationsMessage())) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                if (periodIsOpened()) {
                    hasPendingTransactions();
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean cardPresence(boolean lockCard) {
        while (!sendMessage(new SecurityModuleFunctions().getCardPresenceMessage(lockCard))) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                if (lastReceivedMessage.charAt(6) == '1') {
                    hasCard = true;
                } else {
                    hasCard = false;
                }
                return true;
            } else if (state.equals(registryManager.readValue("TPAErrors", "EXECUTION_ERROR", false))) {
                return false;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public String getMerchantCardData() {
        String receivedBIN = "";
        while (!sendMessage(new SecurityModuleFunctions().getMerchantCardDataMessage())) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                try {
                    receivedBIN = lastReceivedMessage.substring(7, 27);
                } catch (Exception ex) {
                }
                return receivedBIN;
            } else {
                System.out.println("Error getting BIN from magnetic card: " + state);
                return "";
            }
        } else {
            return "";
        }
    }

    public String GetSpecialPAN() {
        String specialPAN = "00000000000000000000";
        byte[] teste = new byte[50];
        while (!sendMessage(new SecurityModuleFunctions().GetDiscretionaryDataMessage())) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                try {
                    if(lastReceivedMessage.length() == 14){
                        specialPAN = lastReceivedMessage.substring(6,14) + "000000000000";
                    }
                    else{
                        specialPAN = lastReceivedMessage.substring(6,14) + "0000" + lastReceivedMessage.substring(14,22);
                        //specialPAN = "0000" + lastReceivedMessage.substring(6, 22);
                    }
                    
                    teste = specialPAN.getBytes();
                    for(int i=0;i<specialPAN.length();++i){
                        if(teste[i] < 48 || teste[i]> 57) teste[i]= 48;
                    }
                    specialPAN = new String(teste);
                    System.out.println("Special PAN: [" + specialPAN + "]");
                } catch (Exception ex) {
                    System.out.println("Error getting special PAN: " + ex.getMessage() + state);
                    return specialPAN;
                }
                return specialPAN;
            } else {
                System.out.println("Error getting special PAN: " + state);
                return specialPAN;
            }
        } else {
            return specialPAN;
        }
    }

    public boolean cardServiceRequest(boolean online, boolean receipt) {
        String message1;
        String message2;
        if (online) {
            if (receipt) {
                message1 = registryManager.readValue("MessagesToTPA", "INSERT_CARD_PT", false);
                message2 = "PARA EMITIR RECIBO";
            } else {
                message1 = registryManager.readValue("MessagesToTPA", "INSERT_CARD_PT", false);
                message2 = registryManager.readValue("MessagesToTPA", "INSERT_CARD_EN", false);
            }
        } else {
            message1 = registryManager.readValue("MessagesToTPA", "OUT_OF_SERVICE_PT", false);
            message2 = registryManager.readValue("MessagesToTPA", "OUT_OF_SERVICE_EN", false);
        }
        while (!sendMessage(new SecurityModuleFunctions().getCardServiceRequestMessage(message1, message2))) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                try {
                    cardType = Integer.parseInt(lastReceivedMessage.substring(6, 7));
                    if (cardType == MAGNETIC) {
                        if (lastReceivedMessage.length() > 10) {
                            lastReceivedBIN = getMerchantCardData();
                            System.out.println("\nMagnetic Card BIN: [" + lastReceivedBIN + "]\n");
                            if (lastReceivedBIN.startsWith("457454") || lastReceivedBIN.equals("")) {
                                lastReceivedBIN = GetSpecialPAN();
                            }
                        }

                    } else if (cardType == CHIP) {
                        System.out.println("CHIP Card");
                        lastReceivedBIN = GetSpecialPAN();                        
                    }
                } catch (ArrayIndexOutOfBoundsException ex) {
                    lastReceivedCard = "";
                }
                message1 = registryManager.readValue("MessagesToTPA", "WAIT_PT", false);
                message2 = registryManager.readValue("MessagesToTPA", "WAIT_EN", false);
                returnToIdleState(message1, message2);
                return true;
            } else if (state.equals(registryManager.readValue("TPAErrors", "READING_ERROR", false))) {
                try {
                    returnToIdleState("ERRO DE LEITURA", "READING ERROR");
                    Thread.sleep(DELAY_BETWEEN_MESSAGES * 6);
                    return false;
                } catch (InterruptedException ex) {
                    return false;
                }
            } else if (state.equals(registryManager.readValue("TPAErrors", "CARD_PRESENT_ON_READER", false))) {
                checkCardPresence(false);
            }
        }
        return false;
    }

    public String GetBINFromChip() {
        while (!sendMessage(new SecurityModuleFunctions().GetBINFromChipMessage())) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                return lastReceivedMessage.substring(6, lastReceivedMessage.length());
            } else {
                System.out.println("Error getting BIN from chip: " + state);
                return "";
            }
        } else {
            return "";
        }
    }

    public boolean performAuthorization() {

        while (!sendMessage(new SecurityModuleFunctions().getPerformAuthorizationMessage(10000))) {
            delayMs(10);
        }
        if (receiveMessage()) {
            System.out.println("Error getting Authorization: " + state);
            if (state.equals("76")) {
                System.out.println("Error getting Authorization: " + state);
                return false;
            } else if (state.equals("69")) {
                System.out.println("Error getting Authorization: " + state);
                return false;
            } else if (state.equals("77")) {
                System.out.println("Error getting Authorization: " + state);
                return false;
            } else if (state.equals("75")) {
                System.out.println("Error getting Authorization: " + state);
                return false;
            } else if (state.equals("72")) {
                System.out.println("Error getting Authorization: " + state);
                return false;
            } else if (state.equals("71")) {
                System.out.println("Error getting Authorization: " + state);
                return false;
            } else if (state.equals("78")) {
                System.out.println("Error getting Authorization: " + state);
                return false;
            } else if (state.equals("XX")) {
                System.out.println("Error getting Authorization: " + state);
                return false;
            } else if (state.equals("80")) {
                System.out.println("Exited by timeout!!!");
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public synchronized boolean performPaymentAfterAuthorizationWithoutReceipt(int value, String authorization, int pumpNumber, int transactionNumber) {
        System.out.println("Performing payment without receipt for value: " + value + " authorization: " + authorization + " and pumpNumber: " + pumpNumber);
        while (!sendMessage(new SecurityModuleFunctions().getPerformPurchaseAfterAuthorizationMessage(value, authorization))) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                return true;
            } else {
                System.out.println("Error performing payment state: " + state);
                return false;
            }
        } else {
            return false;
        }
    }

    public synchronized boolean performPaymentAfterAuthorization(int value, String authorization, int pumpNumber, int transactionNumber, Boolean printMessages) {
        String receipt = "";
        System.out.println("Performing payment for value: " + value + " authorization: " + authorization + " and pumpNumber: " + pumpNumber);
        while (!sendMessage(new SecurityModuleFunctions().getPerformPurchaseAfterAuthorizationMessage(value, authorization))) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals(registryManager.readValue("TPAErrors", "COMMAND_OK", false))) {
                receipt = getLastReceivedMessage().substring(44, getLastReceivedMessage().length());
                receipt = filling.composeReceipt(receipt, printMessages);
                databaseInsertions.insertReceipt(getLastReceivedCard(), receipt);
                return true;
            } else {
                System.out.println("Error performing payment state: " + state);
                //databaseInsertions.insertPendingTransaction(value + "", authorization, pumpNumber + "", transactionNumber + "");
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean returnToIdleState(String message1, String message2) {
        while (!sendMessage(new SecurityModuleFunctions().getReturnToIdleStateMessage(false, message1, message2))) {
            delayMs(10);
        }
        if (receiveMessage()) {
            return true;
        } else {
            return false;
        }
    }

    public String openKeyboardForPumpNumber(int minimumDigits, int maximunDigits, String message1, String message2) {
        while (!sendMessage(new SecurityModuleFunctions().getOpenKeyboardForInfoMessage(minimumDigits, maximunDigits, message1, message2))) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals("00")) {
                return lastReceivedMessage.substring(7, lastReceivedMessage.length());
            } else {
                return "0";
            }
        } else {
            return "";
        }
    }

    public boolean openKeyboardForReceipt(String message1, String message2) {
        while (!sendMessage(new SecurityModuleFunctions().getOpenKeyboardForInfoMessage(0, 0, message1, message2))) {
            delayMs(10);
        }
        if (receiveMessage()) {
            if (state.equals("00")) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean printReceipt(String messageToPrint, Boolean printLogo, Boolean receipt, Boolean generatedDiscount) {
        byte[] centerAll = {0x20, 0x20, 0x1B, 0x61, 0x01};
        byte[] logo1 = {0x1C, 0x70, 0x01, 0x30};
        byte[] logo2 = {0x1C, 0x70, 0x02, 0x30};
        databaseInsertions.insertNewTicket(messageToPrint, receipt, generatedDiscount);
        if (printLogo) {
            sendMessage(new SecurityModuleFunctions().getPrintMessage(new String(centerAll) + "\r\n" + new String(logo1) + "\r\n" + messageToPrint));
        } else {
            sendMessage(new SecurityModuleFunctions().getPrintMessage(new String(centerAll) + "\r\n" + new String(logo2) + "\r\n" + messageToPrint));
        }
        receiveMessage();
        return true;
    }

    public void PrintAllTogether(String receipt, String discountReceipt, boolean generatedDiscount) {
        byte[] centerAll = {0x20, 0x20, 0x1B, 0x61, 0x01};
        byte[] logo1 = {0x1C, 0x70, 0x01, 0x30};
        byte[] logo2 = {0x1C, 0x70, 0x02, 0x30};
        databaseInsertions.insertNewTicket(receipt + discountReceipt, true, generatedDiscount);
        sendMessage(new SecurityModuleFunctions().getPrintMessage(new String(centerAll) + "\r\n" + new String(logo1) + "\r\n" + receipt + "\r\n" + new String(logo2) + "\r\n" + discountReceipt));
        receiveMessage();
    }

    public boolean sendMessage(byte messageToSend[]) {
        int messageSize = 0;
        int index = 0;
        delayMs(DELAY_BETWEEN_MESSAGES);
        while (!tcpclient.OpenSocket()) {
            delayMs(1000);
        }
        while (!(messageToSend[index] == SecurityModuleFunctions.DLE && messageToSend[index + 1] == SecurityModuleFunctions.ETX)) {
            ++messageSize;
            ++index;
        }
        if (mainWindow != null) {
            mainWindow.updateSideState("A enviar mensagem: " + new String(messageToSend).substring(2, messageSize), sideNumber);
        }
        System.out.println("Sending message: " + new String(messageToSend).substring(2, messageSize));
        logger.logString("Sending message: " + new String(messageToSend).substring(2, messageSize), logger.INFO);
        try {
            tcpclient.outputStream.write(messageToSend, 0, messageSize + 3);
        } catch (IOException ex) {
            logger.logException(ex, logger.WARNING);
        }
        if (!getAck()) {
            return false;
        }
        if (mainWindow != null) {
            mainWindow.updateSideState("Mensagem enviada! " + new String(messageToSend).substring(2, messageSize), sideNumber);
        }
        return true;
    }

    public synchronized boolean receiveMessage() {


        InputStream inputStream;

        byte[] receivedMessage = new byte[MAX_ARRAY_SIZE];
        int index = 0;
        int ETXIndex = 0;
        int STXIndex = 0;
        int maxTries = 0;
        byte receivedChar = 0x00;
        inputStream = tcpclient.GetInputStream();
        while (true) {
            try {
                receivedChar = (byte) inputStream.read();
                if (receivedChar != -1) {
                    if (receivedChar == SecurityModuleFunctions.STX && receivedMessage[index - 1] == SecurityModuleFunctions.DLE) {
                        STXIndex = index;
                    }
                    receivedMessage[index++] = receivedChar;
                } else {
                    if (maxTries++ == 1000) {
                        tcpclient.CloseSocket();
                        return false;
                    }
                }
            } catch (IOException ex) {
                tcpclient.CloseSocket();
                return false;
            }

            if (index > 1) {
                if (receivedMessage[index - 1] == SecurityModuleFunctions.ETX && receivedMessage[index - 2] == SecurityModuleFunctions.DLE) {
                    ETXIndex = index - 1;
                    break;

                }
            }
            if (index == MAX_ARRAY_SIZE - 1) {
                tcpclient.CloseSocket();
                return false;
            }

        }

        try {
            receivedMessage[index++] = (byte) tcpclient.GetInputStream().read();
        } catch (IOException ex) {
        }
        try {
            setTpaState(new String(receivedMessage).substring(2, 4));
            lastReceivedMessage = new String(receivedMessage).substring(STXIndex + 1, ETXIndex - 1);
        } catch (ArrayIndexOutOfBoundsException ex) {
            logger.logException(ex, logger.WARNING);
        }
        try {
            tcpclient.outputStream.write(SecurityModuleFunctions.ACK);
        } catch (IOException ex) {
            logger.logException(ex, logger.WARNING);
        }
        tcpclient.CloseSocket();

        if (mainWindow != null) {
            mainWindow.updateSideLastMessageReceived(lastReceivedMessage, sideNumber);
        }

        if (mainWindow != null) {
            mainWindow.updateSideState("Mensagem recebida", sideNumber);
        }
        logger.logString("Message received from side " + sideNumber + ": " + lastReceivedMessage, logger.INFO);
        System.out.println("Message received from side " + sideNumber + ": [" + lastReceivedMessage + "]");
        return true;
    }

    public boolean getAck() {

        try {
            if (tcpclient.GetInputStream().read() == SecurityModuleFunctions.ACK) {
                return true;
            } else {
                return false;
            }
        } catch (IOException ex) {
            logger.logException(ex, logger.WARNING);
            return false;
        }
    }
}
