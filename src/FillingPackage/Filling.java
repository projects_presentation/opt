package FillingPackage;

import DatabasePackage.DatabaseInsertions;
import FileLogger.FileLogger;
import MainPackage.MainWindow;
import MicrelecPackage.Micrelec;
import MicrelecPackage.MicrelecChannel;
import RegistryManager.RegistryManager;
import TPAPackage.SecurityModule;
import VCPackage.Card;
import VCPackage.Promotion;
import VCPackage.VCJumbo;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Filling extends Thread {

    public String lastReceivedCard = "";
    private String transactionId = "";
    private int transactionNumber = 0;
    private int totalAmount = 0;
    private int dispensedVolume = 0;
    private int unitPrice = 0;
    public int productCode = 0;
    private int errorCode = 0;
    private String fillingState = "";
    public String receipt = "";
    private DatabaseInsertions databaseInsertions;
    private SecurityModule securityModule;
    private Micrelec micrelec;
    private MicrelecChannel channel;
    private RegistryManager registryManager;
    private final String REGISTRY_ROOT = "SOFTWARE\\GWorks\\OPT\\";
    private FileLogger logger;
    private int pumpNumber = 0;
    public static final int VALUE_TO_REMOVE_FROM_AUTHORIZATION = 150;
    public static final int MINIMUM_VALUE_FOR_AUTHORIZATION = 300;
    //private Boolean bill = false;
    private Boolean bill2 = false;
    /**
     * ******** FILLING STATES *************
     */
    public final String NOT_STARTED = "NOT_STARTED";
    public final String STARTED = "STARTED";
    public final String FILLING = "FILLING";
    public final String FINISHED = "FINISHED";
    public final String READY_TO_BE_SEND = "READY_TO_BE_SEND";
    public final String ABORTED = "ABORTED";
    /**
     * *********************
     */
    public final String DATABASE_TRANSACTION_NUMBER = "TRANSACTION_NUMBER";
    public final String DATABASE_TRANSACTION_ID = "TRANSACTION_ID";
    public final String DATABASE_FILLING_STATE = "STATE";
    public final String DATABASE_VALUE = "VALUE_EUR";
    public final String DATABASE_VOLUME = "VOLUME_LTS";
    public final String DATABASE_PRICE_PER_UNIT = "PRICE_PER_UNIT";
    public final String DATABASE_PRODUCT_CODE = "PRODUCT_CODE";
    public final String FILLING_FINISHED = "FILLING_FINISHED";
    private MainWindow mainWindow;
    private String PAN = "";
    private String savedReceipt = "";
    private boolean cutReceipt = true;
    private String receiptLineToReplace = "";
    private boolean receiptIsToPrint = false;

    public Filling(SecurityModule securityModule, int pumpNumber, MainWindow mainWindow, String PAN) {

        this.PAN = PAN;

        System.out.println("\nStarting a new filling for card: " + PAN + "\n");

        this.mainWindow = mainWindow;
        this.securityModule = securityModule;
        this.micrelec = securityModule.getMicrelec();
        this.logger = new FileLogger("OPT Lado " + securityModule.getSideNumber(), REGISTRY_ROOT);
        this.pumpNumber = pumpNumber;
        registryManager = new RegistryManager(REGISTRY_ROOT);
        databaseInsertions = new DatabaseInsertions(logger);
        lastReceivedCard = securityModule.getLastReceivedCard();
        transactionNumber = mainWindow.setTransactionNumber();

        channel = new MicrelecChannel(9, mainWindow);
        fillingState = NOT_STARTED;
        System.out.println("Security Module: " + securityModule.getTPANumber() + "Last received card: " + lastReceivedCard + " starting new filling for pump number: " + securityModule.getPumpNumber());
        databaseInsertions.insertNewFilling(transactionNumber, securityModule.getPumpNumber(), securityModule.getTPANumber(), fillingState);
        mainWindow.setCommandNumber(transactionNumber, securityModule.getSideNumber());
        cutReceipt = registryManager.readValue("Settings", "CutReceipt", false).equals("True");
    }

    public SecurityModule getSecurityModule() {
        return securityModule;
    }

    private void updateFillingFinishTimeAndDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String actualDate = dateFormat.format(new Date()).replaceAll("/", "-");
        databaseInsertions.updateFillingValue(FILLING_FINISHED, transactionNumber, actualDate, securityModule.getSideNumber());
    }

    private void delayMs(int delayValue) {
        try {
            Thread.sleep(delayValue);
        } catch (InterruptedException ex) {
            logger.logException(ex, logger.WARNING);
        }
    }

    private void finishFilling(int abortCase, boolean performPurchase) {

        Promotion promotionToApply = securityModule.vCJumbo.applyPromotion(productCode);
        String tpaReceipt = "";
        Card card = new Card("");

        if (promotionToApply == null) {
            if (securityModule.vCJumbo.getPromotionsList() != null) {
                card = securityModule.vCJumbo.getPromotionsList().getCardByBIN(PAN.substring(0, 8));
            }
            if (card == null) {
                card = new Card("");
                card.separateFields("TV0000P");
            }
        } else {
            card = promotionToApply.getCardByBIN(PAN.substring(0, 8));
            if (card == null) {
                card = new Card("");
                card.separateFields("TV0000P");
            }
        }
        mainWindow.updateSideState("A finalizar abastecimento...", securityModule.getSideNumber());
        updateFillingFinishTimeAndDate();
        System.out.println("Finishing filling...");
        if (performPurchase) {
            if (!securityModule.performPaymentAfterAuthorization(totalAmount, transactionId, pumpNumber, transactionNumber, card.getAccountDiscount().equals("C"))) {
                logger.logString("Error performing payment! Last received Message: " + securityModule.lastReceivedMessage, logger.ERROR);
            }
            receipt = securityModule.getLastReceivedMessage().substring(44, securityModule.getLastReceivedMessage().length());
            tpaReceipt = securityModule.getLastReceivedMessage().substring(44, securityModule.getLastReceivedMessage().length());


            receipt = composeReceipt(receipt, card.getAccountDiscount().equals("C"));
            logger.logString("Filling finished value: " + totalAmount + " Transaction ID: " + transactionId, logger.INFO);
            /*
             * Print Receipt
             */
            if (registryManager.readValue("Settings", "Print Receipt", false).equals("False")) {
                /*
                 * Flag desabilitada...
                 */
                receiptIsToPrint = securityModule.openKeyboardForReceipt(registryManager.readValue("MessagesToTPA", "RECEIPT_PT", false), registryManager.readValue("MessagesToTPA", "RECEIPT_EN", false));
                if (receiptIsToPrint) {
                    if (securityModule.vCJumbo.getPromotionsList().BINBelongsToWorker(PAN.substring(0, 8))) {
                        bill2 = securityModule.openKeyboardForReceipt(registryManager.readValue("MessagesToTPA", "BILL_PT", false), "SIM: OK / NAO C");
                    }
                    printReceipt(true);
                } else {
                    if (securityModule.vCJumbo.getPromotionsList().BINBelongsToWorker(PAN.substring(0, 8))) {
                        bill2 = securityModule.openKeyboardForReceipt(registryManager.readValue("MessagesToTPA", "BILL_PT", false), "SIM: OK / NAO C");
                    }
                    receipt = tpaReceipt;
                    printReceipt(true);
                }
            } else {
                /*
                 * Flag habilitada...
                 */
                System.out.println("BIN a testar: " + PAN.substring(0, 8));
                if (securityModule.vCJumbo.getPromotionsList().BINBelongsToWorker(PAN.substring(0, 8))) {
                    System.out.println("BIN de Funcionario: " + PAN.substring(0, 8));
                    bill2 = securityModule.openKeyboardForReceipt(registryManager.readValue("MessagesToTPA", "BILL_PT", false), "SIM: OK / NAO C");
                }
                System.out.println("BIN Normal: " + PAN.substring(0, 8));
                printReceipt(true);
            }
            ApplyPromotion();

        }

        if (abortCase == 1) {
            logger.logString("Aborting filling...", logger.INFO);
            securityModule.returnToIdleState(registryManager.readValue("MessagesToTPA", "OUT_OF_SERVICE_PT", false), registryManager.readValue("MessagesToTPA", "OUT_OF_SERVICE_PT", false));
            delayMs(3000);
        } else if (abortCase == 2) {
            logger.logString("Aborting filling...", logger.INFO);
            securityModule.returnToIdleState(registryManager.readValue("MessagesToTPA", "OPERATION_CANCELED_PT", false), registryManager.readValue("MessagesToTPA", "OPERATION_CANCELED_EN", false));
            delayMs(3000);
        } else if (abortCase == 3) {
            logger.logString("Aborting filling...", logger.INFO);
            fillingState = READY_TO_BE_SEND;
            databaseInsertions.updateFillingValue(DATABASE_FILLING_STATE, transactionNumber, fillingState, securityModule.getSideNumber());
            securityModule.returnToIdleState("OBRIGADO", "O JUMBO AGRADECE");
            delayMs(3000);
            fillingState = ABORTED;
            return;
        }
        channel.setChannelState(MicrelecChannel.state.WAITING_TO_BE_RELEASED);
        fillingState = ABORTED;
        databaseInsertions.updateFillingValue(DATABASE_FILLING_STATE, transactionNumber, fillingState, securityModule.getSideNumber());
    }

    private boolean getPumpToStartFilling() {
        int timeout = 0;
        System.out.println("Waiting to select dispenser");
        mainWindow.updateSideState("A reservar bomba...", securityModule.getSideNumber());
        while (!channel.getChannelState().equals(MicrelecChannel.state.DISPENSER_SELECTED)) {
            if (channel.getChannelState().equals(MicrelecChannel.state.ERROR)) {
                logger.logString("Error reserving pump...", logger.WARNING);
                mainWindow.updateSideState("Erro a reservar bomba!", securityModule.getSideNumber());
                return false;
            }
            if (timeout++ == 10000) {
                logger.logString("Error reserving pump exited by timeout...", logger.WARNING);
                mainWindow.updateSideState("Erro a reservar bomba (Timeout)!", securityModule.getSideNumber());
                return false;
            }
            delayMs(1);
        }
        return true;
    }

    @Override
    public void run() {
        String message;
        if (micrelec.isCommunicating()) {
            channel = micrelec.getNextAvaiableChannel(securityModule.getSideNumber());
            if (channel != null) {
                channel.setPumpNumber(securityModule.getPumpNumber());
                channel.setTransactionNumber(transactionNumber);
                fillingState = STARTED;
                databaseInsertions.updateFillingValue(DATABASE_FILLING_STATE, transactionNumber, fillingState, securityModule.getSideNumber());
            } else {
                logger.logString("Error reserving channel...", logger.WARNING);
                mainWindow.updateSideState("Erro a reservar canal!", securityModule.getSideNumber());
                finishFilling(1, false);
                return;
            }
        } else {
            logger.logString("Error micrelec is not communicating...", logger.INFO);
            mainWindow.updateSideState("Erro micrelec nao comunica!", securityModule.getSideNumber());
            finishFilling(1, false);
            return;
        }
        if (!getPumpToStartFilling()) {
            finishFilling(1, false);
            return;
        }
        mainWindow.updateSideState("A aguardar autorização...", securityModule.getSideNumber());

        if (securityModule.performAuthorization()) {
            message = securityModule.getLastReceivedMessage();
            securityModule.checkCardPresence(false);
            securityModule.lastReceivedMessage = message;
            try {
                channel.setMaxCreditAmount(Integer.parseInt(securityModule.getLastReceivedMessage().substring(54, 62)) - VALUE_TO_REMOVE_FROM_AUTHORIZATION);
                System.out.println("\n\nMax credit amount: " + channel.getMaxCreditAmount() + "\n\n");
                System.out.println("Minimum value for authorization: " + MINIMUM_VALUE_FOR_AUTHORIZATION);
                transactionId = securityModule.getLastReceivedMessage().substring(62, 79);
                databaseInsertions.updateFillingValue(DATABASE_TRANSACTION_ID, transactionNumber, transactionId, securityModule.getSideNumber());
                if (channel.getMaxCreditAmount() <= MINIMUM_VALUE_FOR_AUTHORIZATION) {
                    finishFilling(4, true);
                    return;
                }
            } catch (NumberFormatException ex) {
                System.out.println("Error setting max credit amount or transaction ID: " + ex.getMessage());
                logger.logString("Error setting max credit amount or transaction ID: ", logger.WARNING);
                mainWindow.updateSideState("Erro a ajustar valor máximo!", securityModule.getSideNumber());
                finishFilling(2, true);
                return;
            }
            channel.setChannelState(MicrelecChannel.state.SETTING_MAXIMUN_CREDIT_AMOUNT);
        } else {
            logger.logString("Error starting filling: " + securityModule.getLastReceivedMessage(), logger.WARNING);
            mainWindow.updateSideState("Erro a obter autorização!", securityModule.getSideNumber());
            finishFilling(2, false);
            return;
        }
        securityModule.checkCardPresence(false);

        securityModule.returnToIdleState("ABASTEÇA POR FAVOR ", "NA BOMBA: " + pumpNumber);
        delayMs(3000);
        fillingState = FILLING;
        if (!waitForNoozleRemoving()) {
            finishFilling(2, true);
            return;
        }
        securityModule.returnToIdleState(registryManager.readValue("MessagesToTPA", "PUMP_IN_USE_PT", false), registryManager.readValue("MessagesToTPA", "PUMP_IN_USE_EN", false));


        databaseInsertions.updateFillingValue(DATABASE_FILLING_STATE, transactionNumber, fillingState, securityModule.getSideNumber());
        if (!waitForTransactionCompleted()) {
            /*
             * Envia mensagem a avisar a operação foi anulada
             */
            finishFilling(2, false);
            return;
        }
        setParameters();
        finishFilling(3, true);
        return;
    }

    public void printReceipt(Boolean printLogo) {


        Promotion promotion = securityModule.vCJumbo.applyPromotion(productCode);
        double value = ((double) totalAmount) / 100;
        double volume = ((double) dispensedVolume) / 100;
        double adicionalValue = securityModule.vCJumbo.getPromotionValueCardCustomer(PAN, promotion, volume, value);
        double promotionValue = securityModule.vCJumbo.getPromotionValueNormalCustomer(promotion, volume, value);

        Boolean promotionApplied;

        Card card = null;
        if (promotion == null) {
            card = securityModule.vCJumbo.getPromotionsList().getCardByBIN(PAN.substring(0, 8));
            if (card == null) {
                card = new Card("");
                card.separateFields("TV0000P");
            }
        } else {
            card = promotion.getCardByBIN(PAN.substring(0, 8));
            if (card == null) {
                card = new Card("");
                card.separateFields("TV0000P");
            }
        }

        promotionApplied = promotionValue != 0 ? true : false;

        securityModule.returnToIdleState(registryManager.readValue("MessagesToTPA", "WAIT_PT", false), registryManager.readValue("MessagesToTPA", "WAIT_EN", false));
        if (adicionalValue != 0 || promotionValue != 0) {
            if (card.getAccountDiscount().equals("C")) {
                printVCMessage(false, securityModule.vCJumbo.promotionIsInsideDate(promotion));
            } else {
                printVCMessage(true, securityModule.vCJumbo.promotionIsInsideDate(promotion));
            }
        }
        if (bill2) {
            receipt = receipt.replace(receiptLineToReplace, receiptLineToReplace + "\r\n" + "*** Nao pode emitir Fatura ***");
        }
        if (securityModule.vCJumbo.getPromotionsList().getPromotionsList().isEmpty()) {
            securityModule.printReceipt(receipt, printLogo, true, promotionApplied);
            return;
        }
        if (cutReceipt) {
            securityModule.printReceipt(receipt, printLogo, true, promotionApplied);
        } else {
            savedReceipt = receipt;
        }
    }

    public Boolean waitForTransactionCompleted() {
        int counter = 0;
        while (!channel.getChannelState().equals(MicrelecChannel.state.TRANSACTION_COMPLETED)) {
            if (!micrelec.isCommunicating()) {
                return false;
            }
            if (counter == 60000) {
                counter = 0;
                securityModule.returnToIdleState(registryManager.readValue("MessagesToTPA", "PUMP_IN_USE_PT", false), registryManager.readValue("MessagesToTPA", "PUMP_IN_USE_EN", false));
            }
            delayMs(1);
            counter++;
        }
        return true;
    }

    public Boolean waitForNoozleRemoving() {
        int timeout = 0;
        mainWindow.updateSideState("A aguardar agulheta!", securityModule.getSideNumber());
        while (!channel.getChannelState().equals(MicrelecChannel.state.STARTED_DISPENSING)) {
            /*
             * Sai por timeout não retirou a agulheta
             */
            if (!micrelec.isCommunicating()) {
                mainWindow.updateSideState("Erro micrelec não comunica!", securityModule.getSideNumber());
                return false;
            }
            if (timeout++ == 120000) {
                logger.logString("Aborting transaction noozle not removed...", logger.INFO);
                mainWindow.updateSideState("Agulheta não retirada (Timeout)!", securityModule.getSideNumber());
                return false;
            }
            delayMs(1);
        }
        return true;
    }

    public String composeReceipt(String receiptToCompose, Boolean printMessages) {

        NumberFormat nf = new DecimalFormat("0.00");
        NumberFormat nf2 = new DecimalFormat("0.000");

        String receiptToReturn = "";
        Card card = new Card("");

        try {
            double iva = Integer.parseInt(registryManager.readValue("Settings", "Tax", false));
            iva = iva / 100;
            iva = iva + 1;
            double valor = 0;

            System.out.println("Valor em euros do abastecimento: " + getTotalAmount());
            System.out.println("Valor do IVA" + iva);

            valor = (double) ((double) getTotalAmount() - (getTotalAmount() / iva));
            //receiptToReturn += "\r\n\r\n";
            receiptToReturn += registryManager.readValue("Client", "Address1", false) + "\r\n";
            receiptToReturn += registryManager.readValue("Client", "Address2", false) + "\r\n";
            receiptToReturn += registryManager.readValue("Client", "Address3", false) + "\r\n";
            receiptToReturn += registryManager.readValue("Client", "Address4", false) + "\r\n";
            receiptToReturn += registryManager.readValue("Client", "Address5", false) + "\r\n";
            receiptToReturn += registryManager.readValue("Client", "Address6", false) + "\r\n";
            receiptToReturn += registryManager.readValue("Client", "Address7", false) + "\r\n";
            receiptToReturn += registryManager.readValue("Client", "Address8", false) + "\r\n";

            receiptToReturn += "\r\nRecibo N: " + transactionNumber + "  Bomba N: " + registryManager.readValue("TPA" + securityModule.getSideNumber(), "Pump Number", false);
            receiptToReturn += "\r\nQtd: " + nf.format((double) getDispensedVolume() / 100) + " Lts";
            receiptToReturn += " Prod.: " + getProductCode() + "\r\n";
            receiptToReturn += "P.U.: " + nf2.format((double) getUnitPrice() / 1000) + " EUR ";
            receiptToReturn += " Total: " + nf.format((double) getTotalAmount() / 100) + " EUR\r\n";
            receiptLineToReplace = "IVA incluido " + Integer.parseInt(registryManager.readValue("Settings", "Tax", false)) + "% : " + nf.format((double) valor / 100) + " EUR";
            receiptToReturn += receiptLineToReplace;
            receiptToReturn += receiptToCompose + "\r\n";


            Promotion promotion = securityModule.vCJumbo.applyPromotion(productCode);
            double value = ((double) totalAmount) / 100;
            double volume = ((double) dispensedVolume) / 100;
            double adicionalValue = securityModule.vCJumbo.getPromotionValueCardCustomer(PAN, promotion, volume, value);
            double normalValue = securityModule.vCJumbo.getPromotionValueNormalCustomer(promotion, volume, value);
            int sequenceNumber = Integer.parseInt(registryManager.readValue("Settings", "SequenceNumber", false));

            if (printMessages) {

                if (adicionalValue != 0.0) {
                    if (promotion != null) {
                        if (securityModule.vCJumbo.promotionIsInsideDate(promotion)) {
                            receiptToReturn += promotion.getCCMessage(1).substring(1) + "\r\n";
                            receiptToReturn += promotion.getCCMessage(2).substring(1) + "\r\n";
                            receiptToReturn += promotion.getCCMessage(3).substring(1) + "\r\n";
                        }
                    }

                }
            }

            if (promotion != null) {
                card = promotion.getCardByBIN(PAN.substring(0, 8));
                if (card != null) {
                    System.out.println("\n********************************\nTipo de cartão: [" + card.getAccountDiscount() + "]\n********************************\n");
                }
                if (card == null) {
                    if (normalValue != 0.0 || adicionalValue != 0.0) {
                        if (securityModule.vCJumbo.promotionIsInsideDate(promotion)) {
                            receiptToReturn += "\r\nGANHOU VALE DE COMPRAS \r\n N: " + sequenceNumber;
                        }
                    }
                } else {
                    if (!card.getAccountDiscount().equals(("C"))) {
                        if (normalValue != 0.0 || adicionalValue != 0.0) {
                            if (securityModule.vCJumbo.promotionIsInsideDate(promotion)) {
                                receiptToReturn += "\r\nGANHOU VALE DE COMPRAS \r\n N: " + sequenceNumber;
                                receiptToReturn += "\r\nValor = " + (normalValue + adicionalValue) + " EUR\r\n";
                                receiptToReturn += "(o valor do combustivel\r\n";
                                receiptToReturn += "inclui o Vale de Compras)\r\n";
                            }
                        }
                    } else if (card.getAccountDiscount().equals(("C"))) {
                        if (normalValue != 0.0 || adicionalValue != 0.0) {
                            if (securityModule.vCJumbo.promotionIsInsideDate(promotion)) {
                                receiptToReturn += "\r\nGANHOU VALE DE COMPRAS \r\n N: " + sequenceNumber;
                                receiptToReturn += "\r\nA credito na sua\r\n";
                                receiptToReturn += "Conta Jumbo " + (normalValue + adicionalValue) + " Euros\r\n";
                                receiptToReturn += "(o valor do combustivel\r\n";
                                receiptToReturn += "inclui este credito)\r\n";

                            }
                        }
                    }
                }
            }


        } catch (NumberFormatException ex) {
            logger.logException(ex, logger.WARNING);
        }
        savedReceipt = receiptToReturn;
        return receiptToReturn;
    }

    public void setParameters() {
        int index = 3;
        String aux = "";
        try {
            aux = channel.getLastReceivedMessage();
            setTotalAmount(Integer.parseInt(aux.substring(index, index += 6)));
            setDispensedVolume(Integer.parseInt(aux.substring(index, index += 6)));
            setUnitPrice(Integer.parseInt(aux.substring(index, index += 5)));
            setProductCode(Integer.parseInt(aux.substring(index, index += 2)));
            setErrorCode(Integer.parseInt(aux.substring(index, index += 2)));
        } catch (NumberFormatException ex) {
            logger.logString("Error setting parameters: " + aux, logger.WARNING);
            logger.logException(ex, logger.ERROR);
        }
    }

    public void startTransaction(int transactionNumber) {
        fillingState = STARTED;
        this.transactionNumber = transactionNumber;
        System.out.println("Transaction Number for new Transaction:" + transactionNumber);
    }

    public int getDispensedVolume() {
        return dispensedVolume;
    }

    public void setDispensedVolume(int dispensedVolume) {
        System.out.println("Dispensed Volume: " + dispensedVolume);
        mainWindow.updateLastvolume(dispensedVolume, securityModule.getSideNumber());
        databaseInsertions.updateFillingValue(DATABASE_VOLUME, transactionNumber, dispensedVolume + "", securityModule.getSideNumber());
        this.dispensedVolume = dispensedVolume;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        System.out.println("Error Code: " + errorCode);
        this.errorCode = errorCode;
    }

    public String getProductCode() {
        try {
            if (productCode == Integer.parseInt(registryManager.readValue("Products", "S/C95_CODE", false))) {
                return registryManager.readValue("Products", "S/C95", false);
            } else if (productCode == Integer.parseInt(registryManager.readValue("Products", "S/C98_CODE", false))) {
                return registryManager.readValue("Products", "S/C98", false);
            } else if (productCode == Integer.parseInt(registryManager.readValue("Products", "DIESEL_CODE", false))) {
                return registryManager.readValue("Products", "DIESEL", false);
            } else {
                System.out.println("No such product code: " + this.productCode);
                logger.logString("No such product code: " + this.productCode, logger.INFO);
            }
        } catch (NumberFormatException ex) {
            logger.logException(ex, logger.WARNING);
            return "Nao definido";
        }

        return "Nao definido";
    }

    public void setProductCode(int productCode) {
        System.out.println("Product code: " + productCode);
        databaseInsertions.updateFillingValue(DATABASE_PRODUCT_CODE, transactionNumber, productCode + "", securityModule.getSideNumber());
        this.productCode = productCode;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        System.out.println("Total Amount: " + totalAmount);
        mainWindow.updateLastAmout(totalAmount, securityModule.getSideNumber());
        databaseInsertions.updateFillingValue(DATABASE_VALUE, transactionNumber, totalAmount + "", securityModule.getSideNumber());
        this.totalAmount = totalAmount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        System.out.println("Price per unit: " + unitPrice);
        databaseInsertions.updateFillingValue(DATABASE_PRICE_PER_UNIT, transactionNumber, unitPrice + "", securityModule.getSideNumber());
        this.unitPrice = unitPrice;
    }

    public String getFillingState() {
        return fillingState;
    }

    public void printVCMessage(Boolean vc, boolean promotionInsideValidDate) {

        if (vc) {
            if (promotionInsideValidDate) {
                securityModule.returnToIdleState("VAI BENEFICIAR EM", "VALE DE COMPRAS");
                delayMs(4000);
            }
        } else {
            if (promotionInsideValidDate) {
                securityModule.returnToIdleState("DESCONTO ACUMULADO", "CONTA JUMBO");
                delayMs(4000);
            }
        }
        if (!bill2) {
            securityModule.returnToIdleState("PRETENDE FACTURA?", "P.F.SOLICITE NA CABINE");
            delayMs(4000);
        }


    }

    private void ApplyPromotion() {

        int billWasRequested = bill2 ? 1 : 0;
        VCJumbo vCJumbo = securityModule.vCJumbo;
        double value = ((double) totalAmount) / 100;
        double volume = ((double) dispensedVolume) / 100;
        int sequenceNumber = Integer.parseInt(registryManager.readValue("Settings", "SequenceNumber", false));
        if (sequenceNumber == 100000) {
            sequenceNumber = 0;
        }

        if (PAN.length() == 0) {
            PAN = "00000000000000000000";
        }

        Promotion promotionToApply = securityModule.vCJumbo.applyPromotion(productCode);
        Card card = new Card("");
        if (promotionToApply == null) {
            card = securityModule.vCJumbo.getPromotionsList().getCardByBIN(PAN.substring(0, 8));
            if (card == null) {
                card = new Card("");
                card.separateFields("TV0000P");
            }
        } else {
            card = promotionToApply.getCardByBIN(PAN.substring(0, 8));
        }

        double valueOfPromotion;
        double adicionalValue;

        if (promotionToApply != null) {

            System.out.println("Promotion is avaiable for product: " + productCode);
            if (vCJumbo.promotionIsInsideDate(promotionToApply)) {

                /*
                 * Recolhe os valores das promoções
                 */
                valueOfPromotion = vCJumbo.getPromotionValueNormalCustomer(promotionToApply, volume, value);
                adicionalValue = vCJumbo.getPromotionValueCardCustomer(PAN, promotionToApply, volume, value);
                System.out.println("Promotion is in a valid date");
                System.out.println("Applying promotion: " + promotionToApply.getDescription());
                System.out.println("Promocao a ser aplicada ao consumidor normal: " + valueOfPromotion + " EUR");
                System.out.println("Promocao a ser aplicada ao consumidor com cartao Jumbo: " + adicionalValue + " EUR");

                if (promotionToApply.getAdicionalDiscount()) {
                    if (card != null && card.getAccountDiscount().equals("C")) {
                        vCJumbo.updateDatabaseDiscounts(vCJumbo.getBarCode(), PAN, promotionToApply, transactionNumber, valueOfPromotion, adicionalValue, billWasRequested, totalAmount, productCode, getProductCode(), volume);
                        //registryManager.setValue("Settings", "SequenceNumber", ++sequenceNumber + "", false);
                        if (!savedReceipt.equals("") && receiptIsToPrint) {
                            boolean promotionApplied = valueOfPromotion != 0 && adicionalValue != 0 ? true : false;
                            securityModule.printReceipt(savedReceipt, true, true, promotionApplied);
                        }
                        return;
                    } else if (!PAN.substring(0, 8).equals("00000000")) {
                        vCJumbo.updateDatabaseDiscounts(vCJumbo.getBarCode(), PAN, promotionToApply, transactionNumber, 0.0, 0.0, billWasRequested, totalAmount, productCode, getProductCode(), volume);
                        System.out.println("Promotion is NOT avaiable for card: " + PAN + " but is a private label card");
                    }
                }

                sequenceNumber = Integer.parseInt(registryManager.readValue("Settings", "SequenceNumber", false));

                if (PAN.equals("")) {
                    if (vCJumbo.getPromotionValueNormalCustomer(promotionToApply, volume, value) != 0.0) {
                        receipt = vCJumbo.composeReceiptToPrint(valueOfPromotion, adicionalValue, promotionToApply, sequenceNumber, PAN, transactionNumber);
                        if (cutReceipt) {
                            securityModule.printReceipt(receipt, false, false, false);
                        } else {
                            securityModule.PrintAllTogether(savedReceipt, receipt, true);
                        }

                    }
                    vCJumbo.updateDatabaseDiscounts(vCJumbo.getBarCode(), PAN, promotionToApply, transactionNumber, valueOfPromotion, adicionalValue, billWasRequested, totalAmount, productCode, getProductCode(), volume);
                } else {
                    if (!promotionToApply.getAdicionalDiscount()) {
                        if (vCJumbo.getPromotionValueNormalCustomer(promotionToApply, volume, value) != 0.0) {
                            receipt = vCJumbo.composeReceiptToPrint(valueOfPromotion, adicionalValue, promotionToApply, sequenceNumber, PAN, transactionNumber);
                            if (cutReceipt) {
                                securityModule.printReceipt(receipt, false, false, false);
                            } else {
                                securityModule.PrintAllTogether(savedReceipt, receipt, true);
                            }

                        }
                        vCJumbo.updateDatabaseDiscounts(vCJumbo.getBarCode(), PAN, promotionToApply, transactionNumber, valueOfPromotion, adicionalValue, billWasRequested, totalAmount, productCode, getProductCode(), volume);
                    } else {
                        if (vCJumbo.getPromotionValueNormalCustomer(promotionToApply, volume, value) != 0.0 || vCJumbo.getPromotionValueCardCustomer(PAN, promotionToApply, volume, value) != 0.0) {
                            receipt = vCJumbo.composeReceiptToPrint(valueOfPromotion + adicionalValue, adicionalValue, promotionToApply, sequenceNumber, PAN, transactionNumber);
                            if (cutReceipt) {
                                securityModule.printReceipt(receipt, false, false, false);
                            } else {
                                securityModule.PrintAllTogether(savedReceipt, receipt, true);
                            }
                        }
                        vCJumbo.updateDatabaseDiscounts(vCJumbo.getBarCode(), PAN, promotionToApply, transactionNumber, valueOfPromotion, adicionalValue, billWasRequested, totalAmount, productCode, getProductCode(), volume);
                    }
                }


            }

        } else {
            if(receiptIsToPrint && !savedReceipt.equals("")){
                securityModule.printReceipt(receipt, true, true, false);
            }
            vCJumbo.updateDatabaseDiscounts(vCJumbo.getBarCode(), PAN, promotionToApply, transactionNumber, 0.0, 0.0, billWasRequested, totalAmount, productCode, getProductCode(), volume);
            System.out.println("Promotion is NOT avaiable for product: " + productCode);
        }

    }
}
