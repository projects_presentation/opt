package DatabasePackage;

import FileLogger.FileLogger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQLConnection {

    private String username;
    private String password;
    private Connection connection = null;
    private FileLogger logger;

    private final String REGISTRY_ROOT = "SOFTWARE\\GWorks\\OPT\\";

    public MySQLConnection(String username, String password) {
        this.username = username;
        this.password = password;
        this.logger = new FileLogger("OPT", REGISTRY_ROOT );
    }

    public boolean connect(String serverName, String dataBaseName) {
        try {
            String driverName = "org.gjt.mm.mysql.Driver";
            Class.forName(driverName);
            String url = "jdbc:mysql://" + serverName + "/" + dataBaseName;
            connection = (Connection) DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            logger.logException(ex,logger.ERROR);
        } catch (ClassNotFoundException ex) {
            logger.logException(ex,logger.ERROR);
        }
        return true;
    }

    public void disconnect(){
        try {
            connection.close();
        }
        catch (SQLException ex) {
            logger.logException(ex,logger.WARNING);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public boolean executeStatement(String statementToExecute) {
        try {
            Statement stmt = (Statement) connection.createStatement();
            stmt.executeUpdate(statementToExecute);
        } catch (SQLException ex) {
            logger.logException(ex,logger.WARNING);
            return false;
        }
        return true;

    }
}
