package DatabasePackage;

import FileLogger.FileLogger;

import RegistryManager.RegistryManager;
import VCPackage.DataHandler;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatabaseInsertions {

    private RegistryManager registryManager;
    private MySQLConnection sqlConnection;
    private FileLogger logger;
    private DateFormat dateFormat;
    private String serverName = "";
    private String databaseName = "";
    private final String REGISTRY_ROOT = "SOFTWARE\\GWorks\\OPT\\";

    public DatabaseInsertions(FileLogger logger) {

        this.logger = new FileLogger("Database insertions", REGISTRY_ROOT);
        dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        registryManager = new RegistryManager(REGISTRY_ROOT);
        sqlConnection = new MySQLConnection("root", "admin");
        serverName = registryManager.readValue("Database Connection", "Server Name", false);
        databaseName = registryManager.readValue("Database Connection", "Database Name", false);
    }

    public Boolean insertNewFilling(int transactionNumber, int pumpNumber, String TPANumber, String fillingState) {
        String actualDate = "";
        String statementToExecute = "";

        actualDate = dateFormat.format(new Date()).replaceAll("/", "-");
        sqlConnection.connect(serverName, databaseName);
        statementToExecute = "INSERT INTO fillings ("
                + "TRANSACTION_NUMBER"
                + ",TIME_AND_DATE"
                + ",PUMP_NUMBER"
                + ",TPA_NUMBER"
                + ",STATE"
                + ") VALUES ('"
                + transactionNumber + "','"
                + actualDate + "','"
                + pumpNumber + "','"
                + TPANumber + "','"
                + fillingState + "')";
        sqlConnection.executeStatement(statementToExecute);
        sqlConnection.disconnect();


        return true;
    }

    public Boolean insertNewTicket(String ticket,Boolean receipt,Boolean generatedDiscount) {
        String statementToExecute = "";

        String auxiliarString = "";

        Calendar actualDate = Calendar.getInstance();//new DataHandler().getDate();
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String actualDateInString = dateFormat2.format(actualDate.getTime());

        if(receipt && generatedDiscount){
            auxiliarString = "\r\nEste talão imprimiu vale de compras\r\n";
        }

        sqlConnection.connect(serverName, databaseName);
        statementToExecute = "INSERT INTO discount_tickets ("
                + "TIME_AND_DATE"
                + ",RECEIPT"
                  + ") VALUES ('"
                + actualDateInString + "','"
                 + auxiliarString + ticket + "')";
        sqlConnection.executeStatement(statementToExecute);
        sqlConnection.disconnect();


        return true;
    }

    public void updateFillingValue(String columnName, int transactionNumber, String newValue, int sideNumber) {

        Statement statement;
        try {
            System.out.println("Updating Side: " + sideNumber + " Filling Value : " + columnName + " for transaction: " + transactionNumber + " to: " + newValue);
            logger.logString("Updating Side: " + sideNumber + " Filling Value :" + columnName + " for transaction: " + transactionNumber + " to: " + newValue, logger.INFO);
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            statement.executeUpdate("UPDATE fillings SET " + columnName + " = '" + newValue + "' WHERE TRANSACTION_NUMBER = '" + transactionNumber + "' ORDER BY ID DESC LIMIT 1");
            sqlConnection.disconnect();
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            logger.logException(ex, logger.WARNING);
        }

    }

    public void updateFillingValueByTransactionID(String columnName, String transactionNumber, String newValue) {

        Statement statement;
        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            statement.executeUpdate("UPDATE fillings SET " + columnName + " = '" + newValue + "' WHERE TRANSACTION_NUMBER = '" + Integer.parseInt(transactionNumber) + "' ORDER BY ID DESC LIMIT 1");
            sqlConnection.disconnect();
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            logger.logException(ex, logger.WARNING);
        }

    }

    public void updateFillingValueByCommandNumber(String columnName, String commandNumber, String newValue) {

        Statement statement;
        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            statement.executeUpdate("UPDATE fillings SET " + columnName + " = '" + newValue + "' WHERE COMMAND_NUMBER = '" + Integer.parseInt(commandNumber) + "' AND STATE = 'READY_TO_BE_SEND' OR STATE = 'FINISHED' ORDER BY ID DESC LIMIT 1");
            sqlConnection.disconnect();
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            logger.logException(ex, logger.WARNING);
        }

    }

    public String getFillingForSiac() {
        Statement statement;
        ResultSet resultSet;
        String TPANumber = "";
        String pumpNumber = "";
        String value = "";
        String volume = "";
        String pricePerUnit = "";
        String productCode = "";
        String optNumber = "";
        String messageToReturn = "";
        String transactionNumber = "";
        NumberFormat commandNumberFormat = new DecimalFormat("00");
        NumberFormat TPANumberFormat = new DecimalFormat("000000000");
        NumberFormat valueFormat = new DecimalFormat("00000000");
        NumberFormat volumeFormat = new DecimalFormat("0000000");
        NumberFormat privePerUnitFormat = new DecimalFormat("00000");
        NumberFormat transactionNumberFormat = new DecimalFormat("0000");
        NumberFormat productCodeFormat = new DecimalFormat("00");

        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            resultSet = (ResultSet) statement.executeQuery("SELECT * FROM fillings where (STATE = 'READY_TO_BE_SEND' OR STATE = 'FINISHED') AND VALUE_EUR != '0' ORDER BY ID DESC LIMIT 1");
            resultSet.last();
            TPANumber = resultSet.getString("TPA_NUMBER");
            pumpNumber = resultSet.getString("PUMP_NUMBER");
            value = resultSet.getString("VALUE_EUR");
            volume = resultSet.getString("VOLUME_LTS");
            pricePerUnit = resultSet.getString("PRICE_PER_UNIT");
            productCode = resultSet.getString("PRODUCT_CODE");
            optNumber = registryManager.readValue("Settings", "OPT Number", false);
            transactionNumber = resultSet.getString("TRANSACTION_NUMBER");
            sqlConnection.disconnect();

            messageToReturn = "M"
                    + optNumber
                    + "00000053"
                    + commandNumberFormat.format(Integer.parseInt(getCommandNumberByTransactionNumber(transactionNumber)))
                    + "2011"
                    + TPANumberFormat.format(Integer.parseInt(TPANumber))
                    + "000000001"
                    + productCodeFormat.format(Integer.parseInt(pumpNumber))
                    + productCodeFormat.format(Integer.parseInt(pumpNumber))
                    + valueFormat.format(Integer.parseInt(value))
                    + volumeFormat.format(Integer.parseInt(volume))
                    + privePerUnitFormat.format(Integer.parseInt(pricePerUnit))
                    + productCodeFormat.format(Integer.parseInt(productCode))
                    + transactionNumberFormat.format(Integer.parseInt(transactionNumber)) + "0";


            return messageToReturn;
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            return "";
        } catch (NumberFormatException ex) {
            sqlConnection.disconnect();
            return "";
        }


    }

    public void insertPendingTransaction(String value, String transactionID, String dispenser, String transactionNumber) {
        
        String actualDate = "";
        String statementToExecute = "";

        dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        actualDate = dateFormat.format(new Date()).replaceAll("/", "-");

        sqlConnection.connect(serverName, databaseName);
        statementToExecute = "INSERT INTO pending_transactions ("
                + "VALUE"
                + ",TRANSACTION_ID"
                + ",TIME_AND_DATE"
                + ",DISPENSER"
                + ",STATE"
                + ",TRANSACTION_NUMBER"
                + ") VALUES ('"
                + value + "','"
                + transactionID + "','"
                + actualDate + "','"
                + dispenser + "','"
                + "PENDING'" + ",'"
                + transactionNumber + "')";
        System.out.println("Statement:" + statementToExecute.toString());
        sqlConnection.executeStatement(statementToExecute);
        sqlConnection.disconnect();
    }

    public void removeLastReceipt(String Card) {
        Statement statement;
        ResultSet resultSet;

        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            resultSet = (ResultSet) statement.executeQuery("SELECT * FROM receipts where CARD = '" + Card + "' ORDER BY ID DESC LIMIT 1");
            resultSet.last();
            statement.executeUpdate("DELETE FROM receipts where ID = '" + resultSet.getString(1) + "'");
            sqlConnection.disconnect();
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            logger.logException(ex, logger.WARNING);
        }
    }

    public String retreiveReceipt(String Card) {
        Statement statement;
        ResultSet resultSet;
        String receipt = "";
        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            resultSet = (ResultSet) statement.executeQuery("SELECT * FROM receipts where CARD = '" + Card + "' ORDER BY ID DESC LIMIT 1");
            resultSet.last();
            receipt = resultSet.getString(3);
            sqlConnection.disconnect();
            return receipt;
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            return "";
        }
    }

    public String getIDOfPendingTransaction(int pumpNumber) {
        Statement statement;
        ResultSet resultSet;
        String ID = "";
        NumberFormat pumpNumberFormat = new DecimalFormat("00");

        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            resultSet = (ResultSet) statement.executeQuery("SELECT * FROM pending_transactions where DISPENSER = '" + pumpNumberFormat.format(pumpNumber) + "' AND STATE='PENDING' ORDER BY ID DESC LIMIT 1");
            resultSet.last();
            ID = resultSet.getString(1);
            sqlConnection.disconnect();
            return ID;
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            return "";
        }
    }

    public String getTransactionIDByID(String ID) {
        Statement statement;
        ResultSet resultSet;
        String trasactionID = "";
        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            resultSet = (ResultSet) statement.executeQuery("SELECT * FROM pending_transactions where ID = '" + ID + "' ORDER BY ID DESC LIMIT 1");
            resultSet.last();
            trasactionID = resultSet.getString(3);
            sqlConnection.disconnect();
            return trasactionID;
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            logger.logException(ex, logger.WARNING);
            return "";
        }
    }

    public String getValueByID(String ID) {
        Statement statement;
        ResultSet resultSet;
        String value = "";
        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            resultSet = (ResultSet) statement.executeQuery("SELECT * FROM pending_transactions where ID = '" + ID + "' ORDER BY ID DESC LIMIT 1");
            resultSet.last();
            value = resultSet.getString(2);
            sqlConnection.disconnect();
            return value;
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            logger.logException(ex, logger.WARNING);
            return "";
        }
    }

    public String getTransactionNumberByTransactionID(String transactionID) {
        Statement statement;
        ResultSet resultSet;
        String transactionNumber = "";
        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            resultSet = (ResultSet) statement.executeQuery("SELECT * FROM pending_transactions where TRANSACTION_ID = '" + transactionID + "' ORDER BY ID DESC LIMIT 1");
            resultSet.last();
            transactionNumber = resultSet.getString("TRANSACTION_NUMBER");
            sqlConnection.disconnect();
            return transactionNumber;
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            logger.logException(ex, logger.WARNING);
            return "";
        }
    }

    public String getTransactionIDByTransactionNumber(String transactionNumber) {
        Statement statement;
        ResultSet resultSet;
        String trasactionID = "";
        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            resultSet = (ResultSet) statement.executeQuery("SELECT * FROM fillings where TRANSACTION_NUMBER = '" + Integer.parseInt(transactionNumber) + "' ORDER BY ID DESC LIMIT 1");
            resultSet.last();
            trasactionID = resultSet.getString("TRANSACTION_ID");
            sqlConnection.disconnect();

            return trasactionID;
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            logger.logException(ex, logger.WARNING);
            return "";
        }
    }

    public void updatePendingTransactionByID(String ID) {

        Statement statement;
        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            statement.executeUpdate("UPDATE pending_transactions SET STATE = 'PAID' WHERE ID = '" + ID + "' ORDER BY ID DESC LIMIT 1");
            sqlConnection.disconnect();
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            logger.logException(ex, logger.WARNING);
        }

    }

    public void updateFillingAfterAborted(String transactionNumber, String value, String volume, String productCode, String pricePerUnit) {

        Statement statement;
        String update = "";

        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            update = "UPDATE fillings SET STATE = 'FINISHED',"
                    + "VALUE_EUR = '" + value + "',"
                    + "VOLUME_LTS = '" + volume + "',"
                    + "PRODUCT_CODE = '" + productCode + "',"
                    + "PRICE_PER_UNIT = '" + pricePerUnit
                    + "' WHERE TRANSACTION_NUMBER = '" + Integer.parseInt(transactionNumber) + "' ORDER BY ID DESC LIMIT 1";
            System.out.println(update);
            statement.executeUpdate(update);
            sqlConnection.disconnect();
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            logger.logException(ex, logger.WARNING);
        }

    }

    public void insertReceipt(String lastReadedCard, String receiptToInsert) {
        String actualDate;
        actualDate = dateFormat.format(new Date()).replaceAll("/", "-");
        sqlConnection.connect(serverName, databaseName);
        sqlConnection.executeStatement("INSERT INTO receipts ("
                + "CARD"
                + ",RECEIPT"
                + ",TIME_AND_DATE"
                + ") VALUES ('"
                + lastReadedCard + "','"
                + receiptToInsert + "','"
                + actualDate + "')");
        System.out.println("Inserting new receipt: " + "INSERT INTO receipts (" + "CARD" + ",RECEIPT"
                + ",TIME_AND_DATE"
                + ") VALUES ('"
                + lastReadedCard + "','"
                + receiptToInsert + "','"
                + actualDate + "')");
        sqlConnection.disconnect();
    }

    public String getCommandNumberByTransactionNumber(String transactionNumber) {
        Statement statement;
        ResultSet resultSet;
        String commandNumber = "";
        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            resultSet = (ResultSet) statement.executeQuery("SELECT * FROM fillings where TRANSACTION_NUMBER = '" + Integer.parseInt(transactionNumber) + "' ORDER BY ID DESC LIMIT 1");
            resultSet.last();
            commandNumber = resultSet.getString("COMMAND_NUMBER");
            sqlConnection.disconnect();

            return commandNumber;
        } catch (SQLException ex) {
            sqlConnection.disconnect();
            logger.logException(ex, logger.WARNING);
            return "";
        }
    }
}
