package VCPackage;

import DatabasePackage.MySQLConnection;
import RegistryManager.RegistryManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;

public class TCPServer extends Thread {

    // <editor-fold defaultstate="collapsed" desc="Global Variables">
    private Socket socket = null;
    private BufferedReader in = null;
    private PrintWriter out = null;
    private String receivedMessage = null;
    private Promotion promotion = null;
    private Product product = null;
    private Store store = null;
    private PromotionsList promotionsList = null;
    private Card card = null;
    private static final String REGISTRY_ROOT = "SOFTWARE\\GWorks\\OPT\\";
    RegistryManager registryManager = new RegistryManager(REGISTRY_ROOT);
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Contructor">
    public TCPServer(Socket socket, PromotionsList promotionsList) {

        this.socket = socket;
        this.promotionsList = promotionsList;
    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Received Messages Handling Functions">
    private void separateElements(String element) {
        ExportPromotionsData export = null;
        store = promotionsList.getStore();
        if (store == null) {
            store = new Store("L000SEM LOJA DEFINIDA             ");
        }
        if (handleCardMessages(element)) {
            return;
        }
        if (element.equals("FINISH")) {
            promotionsList.addPromotion(promotion);
            promotionsList.writePromotionsListToFile();
            out.println("OK");
            out.flush();
        }/* else if (element.startsWith("EXPORTP")) {
        export = new ExportPromotionsData(store.getNumber(), element.substring(7, 13));
        export.exportAllData();
        return;
        } else if (element.startsWith("EXPORTT")) {
        export = new ExportPromotionsData(store.getNumber(), element.substring(7, 13));
        export.ExportAllTickets();
        return;
        
        } */ 
        else if (element.startsWith("F2STATUS")) {
            if (registryManager.readValue("Settings", "CutReceipt", false).equals("True")) {
                out.println("ENABLED");
                System.out.println("Flag enabled");
            } else {
                out.println("DISABLED");
                System.out.println("Flag disabled");
            }
            out.flush();
            return;

        } 
        else if (element.startsWith("FSTATUS")) {
            if (registryManager.readValue("Settings", "Print Receipt", false).equals("True")) {
                out.println("ENABLED");
                System.out.println("Flag enabled");
            } else {
                out.println("DISABLED");
                System.out.println("Flag disabled");
            }
            out.flush();
            return;

        } else if (element.startsWith("FLAGE") || element.startsWith("FLAGD")) {
            if (element.equals("FLAGENABLE")) {
                registryManager.setValue("Settings", "Print Receipt", "True", false);
            } else {
                registryManager.setValue("Settings", "Print Receipt", "False", false);
            }
            out.println("OK");
            out.flush();
            return;
        } else if (element.startsWith("FLAG2")) {
            if (element.equals("FLAG2ENABLE")) {
                registryManager.setValue("Settings", "CutReceipt", "True", false);
            } else {
                registryManager.setValue("Settings", "CutReceipt", "False", false);
            }
            out.println("OK");
            out.flush();
            return;
        } else if (element.startsWith("SENDALLPROMOTIONS")) {
            SendAllPromotionsFromDatabase();
            out.println("OK");
            out.flush();
            return;
        } else if (element.startsWith("FINISHPROMOTION")) {
            promotionsList.writePromotionsListToFile();
            out.println("OK");
            out.flush();
        } else if (element.startsWith("D")) {
            promotion = new Promotion(element.substring(1));
        } else if (element.startsWith("R")) {
            promotionsList.removePromotionByDescription(element.substring(1));
        } else if (element.startsWith("P")) {
            promotion.separateFields(element);
        } else if (element.startsWith("A")) {
            product = new Product(element);
            promotion.addProduct(product);
        } else if (element.startsWith("L")) {
            store = new Store(element);
            promotionsList.setStore(store);
        } else if (element.startsWith("M")) {
            promotion.addReceiptMessage(element);
        } else if (element.startsWith("C")) {
            promotion.addCurrentAccountMessage(element);
        } else if (element.startsWith("G")) {
            sendAllPromotionsAndCards();
        } else if (element.startsWith("X")) {
            sendJustPromotionSettings(promotionsList.getPromotionByDescription(element.substring(1, element.length())));
        } else if (element.startsWith("B")) {
            sendAllPromotionsSettings(promotionsList.getPromotionByDescription(element.substring(1, element.length())));
        } else if (element.startsWith("Z")) {
            sendAllCardDescriptionAndType(element.substring(1));
        }



    }

    private Boolean handleCardMessages(String cardMessage) {


        String cardDescription = "";
        String cardBIN = "";
        String cardSettings = "";

        System.out.println("Handling Card Messages: " + cardMessage);

        if (cardMessage.startsWith("NEWCARD")) {
            cardDescription = cardMessage.substring(7);
            card = new Card(cardDescription);
            promotionsList.addCard(card);
            return true;
        } else if (cardMessage.startsWith("REMCARD")) {
            promotionsList.removeCardByDescription(cardMessage.substring(7));
            return true;
        } else if (cardMessage.startsWith("REMPROMOCARD")) {
            promotion.removeCardByDescription(cardMessage.substring(12));
            return true;
        } else if (cardMessage.startsWith("PROMOTION")) {
            System.out.println("New promotion received: " + cardMessage.substring(9));
            promotion = promotionsList.getPromotionByDescription(cardMessage.substring(9));
            return true;
        } else if (cardMessage.startsWith("REMPROMO")) {
            promotionsList.removePromotionByDescription(cardMessage.substring(9));
            return true;
        } else if (cardMessage.startsWith("ADDCARD")) {
            cardSettings = cardMessage.substring(7, 14);
            cardDescription = cardMessage.substring(14, cardMessage.length());
            card = new Card(cardDescription);
            card.listOfBINs = promotionsList.GetCardByDescription(cardDescription).listOfBINs;
            card.separateFields(cardSettings);
            promotion.AssociateCard(card);
            return true;
        } else if (cardMessage.startsWith("ADDBI")) {
            cardBIN = cardMessage.substring(6, 14);
            cardDescription = cardMessage.substring(14, cardMessage.length());
            card = promotionsList.GetCardByDescription(cardDescription);
            if (cardMessage.startsWith("ADDBIW")) {
                promotionsList.AddWorkerBIN(cardBIN);
            }
            card.AddBIN(cardBIN);
            return true;

        } else if (cardMessage.startsWith("REMBIN")) {
            cardBIN = cardMessage.substring(6, 14);
            cardDescription = cardMessage.substring(14, cardMessage.length());
            card = promotionsList.GetCardByDescription(cardDescription);
            card.RemoveBIN(cardBIN);
            promotionsList.RemoveWorkerBIN(cardBIN);
            return true;
        } else if (cardMessage.equals("FINCARD")) {
            promotionsList.writePromotionsListToFile();
            out.println("OK");
            out.flush();
            return true;
        }
        return false;
    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Cards Functions">
    private void sendAllCardDescriptionAndType(String cardDescription) {

        String BINToSend = "";
        Card auxiliarCard = promotionsList.GetCardByDescription(cardDescription);
        Iterator iterator = auxiliarCard.listOfBINs.iterator();

        out.println(auxiliarCard.GetDescription());
        out.flush();

        /*Send BINs*/
        System.out.println("Sending BINs...");
        while (iterator.hasNext()) {
            BINToSend = iterator.next().toString();
            System.out.println("Sending BIN: " + BINToSend);
            if (promotionsList.BINBelongsToWorker(BINToSend)) {
                out.println(BINToSend + " - Funcionario");
            } else {
                out.println(BINToSend);
            }
            out.flush();
        }
        System.out.println("BINs where ...");
        out.println("");
        out.flush();
    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Promotions Functions">
    private void sendJustPromotionSettings(Promotion promotionToSend) {
        Iterator iterator = null;
        Product productIterated = null;
        out.println(promotionToSend.toString());
        out.flush();
        iterator = promotionToSend.getListOfProducts().iterator();
        while (iterator.hasNext()) {
            productIterated = (Product) iterator.next();
            out.println(productIterated.toString());
            out.flush();
        }
        out.println("");
        out.flush();
    }

    private void sendAllPromotionsSettings(Promotion promotionToSend) {
        Product productIterated = null;
        String message = null;
        Card cardIterated = null;
        Iterator iterator = null;

        out.println(promotionToSend.toString());
        out.flush();

        /*Send all products*/
        iterator = promotionToSend.getListOfProducts().iterator();
        while (iterator.hasNext()) {
            productIterated = (Product) iterator.next();
            out.println(productIterated.toString());
            out.flush();
        }
        /*Send all cards*/
        iterator = promotionToSend.getCardsList().iterator();
        while (iterator.hasNext()) {
            cardIterated = (Card) iterator.next();
            out.println("CARDASS" + cardIterated.GetDescription());
            out.flush();
            System.out.println("Sending card definitions: " + cardIterated.toString());
            out.println("CARDSDEF" + cardIterated.toString());
            out.flush();
        }

        iterator = promotionToSend.getListOfVCMessages().iterator();
        while (iterator.hasNext()) {
            message = (String) iterator.next();
            System.out.println("Sending VC Message: " + message);
            out.println("VCMSG" + message);
            out.flush();
        }

        iterator = promotionToSend.getListOfCCMessages().iterator();
        while (iterator.hasNext()) {
            out.println("CCMSG" + iterator.next());
            out.flush();
        }
        out.println("");
        out.flush();

    }

    private void SendAllPromotionsFromDatabase() {

        ResultSet resultSet;
        MySQLConnection sqlConnection = new MySQLConnection("root", "admin");
        String serverName = registryManager.readValue("Database Connection", "Server Name", false);
        String databaseName = registryManager.readValue("Database Connection", "Database Name", false);
        Statement statement;
        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            resultSet = (ResultSet) statement.executeQuery("SELECT * FROM promotions");
            while (resultSet.next()) {
                out.println(formatStringToSendPromotion(resultSet));
                out.flush();
            }
        } catch (SQLException ex) {
        }
    }

    private String formatStringToSendPromotion(ResultSet resultSet) {
        String stringToReturn = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyHHmm");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("ddMMyy");
        NumberFormat numberFormat = new DecimalFormat("000");
        NumberFormat numberFormat2 = new DecimalFormat("000000");

        Calendar promotionTimeAndDate = Calendar.getInstance();


        try {
            promotionTimeAndDate.setTime(resultSet.getTimestamp("PROMOTION_TIME_AND_DATE"));
            stringToReturn += resultSet.getString("TYPE_OF_DISCOUNT");
            stringToReturn += resultSet.getString("EAN_CODE");
            stringToReturn += resultSet.getString("PAN");
            stringToReturn += numberFormat.format(Integer.parseInt(resultSet.getString("STORE_NUMBER")));
            stringToReturn += resultSet.getString("PROMOTION_CODE");
            stringToReturn += dateFormat.format(promotionTimeAndDate.getTime());
            stringToReturn += numberFormat.format(Integer.parseInt(resultSet.getString("OPT_NUMBER")));
            stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("SEQUENCE_NUMBER")));
            stringToReturn += "999999";
            stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("PROMOTION_VALUE").replaceAll(",", "")));
            stringToReturn += dateFormat2.format(resultSet.getDate("START_DATE"));
            stringToReturn += dateFormat2.format(resultSet.getDate("END_DATE"));
            stringToReturn += "00";
            stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("ADICIONAL_DISCOUNT").replaceAll(",", "")));
            stringToReturn += "1";

        } catch (SQLException ex) {
            System.out.println("Error exporting promotions: " + ex);
        }
        return stringToReturn;

    }

    private void sendAllPromotionsAndCards() {
        System.out.println("Sending promotions...");
        Iterator iterator = promotionsList.getPromotionsList().iterator();
        Promotion promotionIterated = null;
        Card cardIterated = null;

        while (iterator.hasNext()) {
            promotionIterated = (Promotion) iterator.next();
            System.out.println("Promotion description: " + promotionIterated.getDescription());
            out.println("P" + promotionIterated.getDescription());
            out.flush();
        }

        iterator = promotionsList.getAllCardsList().iterator();
        while (iterator.hasNext()) {
            cardIterated = (Card) iterator.next();
            System.out.println("Card description: " + cardIterated.GetDescription());
            out.println("T" + cardIterated.GetDescription());
            out.flush();
        }

        if (promotionsList.getStore() != null) {
            out.println("L" + promotionsList.getStore().toString());
            out.flush();
        }

        out.println("");
        out.flush();
    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Thread Run Function">
    @Override
    public void run() {
        System.out.println("Servidor a executar...");
        try {
            try {
                out = new PrintWriter(socket.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                socket.setSoTimeout(2000);
            } catch (IOException ex) {
                System.out.println("Error while running TCP Server: " + ex);
                return;
            }
            while (true) {
                receivedMessage = (String) in.readLine();
                if (receivedMessage != null) {
                    System.out.println("Received message: " + receivedMessage);
                    separateElements(receivedMessage);
                } else if (receivedMessage.equals("")) {
                    socket.close();
                }
            }
        } catch (Exception ex) {
            System.out.println("O Cliente desligou-se!" + ex.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
                if (socket != null) {
                    socket.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }// </editor-fold>
}
