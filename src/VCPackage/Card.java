package VCPackage;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

public class Card implements Serializable {

    ArrayList<String> listOfBINs = new ArrayList<String>();
    private String description = "";
    private String discountFlag = "P";
    private double discountUnit = 0.0;
    private String accountDiscount = "";

    public Card(String description) {
        this.description = description;
    }

    public String GetDiscountFlag() {
        return discountFlag;
    }

    public double getDiscountUnit() {
        return discountUnit;
    }

    public void separateFields(String cardReg) {

        int index = 1;
        try {
            discountFlag = cardReg.substring(index, index += 1);
            discountUnit = Double.parseDouble(cardReg.substring(index, index += 4)) / 100;
            accountDiscount = cardReg.substring(index, index += 1);
            System.out.println("Card discount unit:" + discountUnit);
        } catch (Exception exception) {
            System.out.println("Error separating card: " + cardReg + "\n" + exception.getMessage());
        }
    }

    public String getAccountDiscount() {
        return accountDiscount;
    }

    @Override
    public String toString() {
        return "C" + discountFlag + new DecimalFormat("00.00").format(discountUnit).replaceAll(",", "").replaceAll("\\.", "") + accountDiscount;
    }

    public String GetDescription() {
        return description;
    }

    public Boolean BINIsInTheList(String BIN) {
        String iteratedBIN;
        Iterator iterator = listOfBINs.iterator();
        while (iterator.hasNext()) {
            iteratedBIN = (String) iterator.next();
            if (iteratedBIN.equals(BIN)) {
                return true;
            }
        }
        return false;
    }

    public void AddBIN(String BIN) {
        System.out.println("A associar o BIN: "+BIN+" ao cartao: " + description);
        listOfBINs.add(BIN);
   }

    public void RemoveBIN(String BIN) {
        Iterator iterator = listOfBINs.iterator();
        while (iterator.hasNext()) {
            String BINToRemove = (String) iterator.next();
            if (BINToRemove.equals(BIN)) {
                listOfBINs.remove(BINToRemove);
                return;
            }
        }


    }
}
