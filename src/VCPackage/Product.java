package VCPackage;

import java.io.Serializable;
import java.text.DecimalFormat;

public class Product implements Serializable{

    private final int diesel = 3;
    private final int fuel95 = 5;
    private final int fuel98 = 8;

    private String promotionCode = null;
    private int type = 0;
    private String EANCode = null;

    public Product(String productReg){
        separateFields(productReg);
    }

    private void separateFields(String productReg){
        int index = 1;
        promotionCode = productReg.substring(index,index+=8);
        type = Integer.parseInt(productReg.substring(index,index+=2));
        EANCode = productReg.substring(index,index+=13);
    }

    @Override
    public String toString(){
        return "A" +
               promotionCode +
               new DecimalFormat("00").format(type) +
               EANCode;
    }

    public String getEANCode() {
        return EANCode;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public int getType() {
        return type;
    }


}
