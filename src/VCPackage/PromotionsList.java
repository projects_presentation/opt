package VCPackage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class PromotionsList implements Serializable {

    private ArrayList<Promotion> listOfPromotions = new ArrayList<Promotion>();
    private ArrayList<Card> listOfAllCards = new ArrayList<Card>();
    private ArrayList<String> listOfWorkersBINs = new ArrayList<String>();
    private Store store = null;
    private final String fileName = "promo.prm";

    public PromotionsList() {
    }

    public void AddWorkerBIN(String BIN) {
        listOfWorkersBINs.remove(BIN);
        listOfWorkersBINs.add(BIN);
    }

    public void RemoveWorkerBIN(String BIN) {
        listOfWorkersBINs.remove(BIN);
    }

    public Boolean BINBelongsToWorker(String BIN) {
        Iterator iterator = listOfWorkersBINs.iterator();
        String iteratedBIN = null;
        while (iterator.hasNext()) {
            iteratedBIN = (String) iterator.next();
            if (iteratedBIN.equals(BIN)) {
                return true;
            }
        }
        return false;
    }

    public Card getCardByBIN(String cardBIN) {
        try {
            Iterator cardsIterator = listOfAllCards.iterator();
            Iterator BINsIterator = null;
            Card cardIterated;
            String BINItarated;
            while (cardsIterator.hasNext()) {
                cardIterated = (Card) cardsIterator.next();
                BINsIterator = cardIterated.listOfBINs.iterator();
                while (BINsIterator.hasNext()) {
                    BINItarated = (String) BINsIterator.next();
                    if (BINItarated.equals(cardBIN.substring(0, 8))) {
                        return cardIterated;
                    }
                }
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    public ArrayList getAllCardsList() {
        return listOfAllCards;
    }

    public ArrayList getListOfBINs() {
        ArrayList listOfBINsToReturn = new ArrayList();
        Card cardIterated = null;
        String BINIterated = null;
        Iterator cardsIterator = listOfAllCards.iterator();
        Iterator BINsIterator = null;
        while (cardsIterator.hasNext()) {
            cardIterated = (Card) cardsIterator.next();
            BINsIterator = cardIterated.listOfBINs.iterator();
            while (BINsIterator.hasNext()) {
                BINIterated = (String) BINsIterator.next();
                listOfBINsToReturn.add(BINIterated);
            }
        }

        return listOfBINsToReturn;
    }

    private void showAllCards() {
        Card cardIterated = null;
        int index = 0;
        Iterator iterator = listOfAllCards.iterator();

        System.out.println("All cards list...");

        while (iterator.hasNext()) {
            cardIterated = (Card) iterator.next();
            System.out.println("Card " + (++index) + " description: " + cardIterated.GetDescription());
        }

        System.out.println("End cards list...");
    }

    public void addCard(Card card) {
        removeCardByDescription(card.GetDescription());
        listOfAllCards.add(card);
        System.out.println("New card added: " + card.GetDescription());
        showAllCards();

    }

    public Card GetCardByDescription(String cardDescription) {
        Card cardIterated = null;
        Iterator iterator = listOfAllCards.iterator();
        while (iterator.hasNext()) {
            cardIterated = (Card) iterator.next();
            if (cardIterated.GetDescription().equals(cardDescription)) {
                return cardIterated;
            }
        }
        return null;

    }

    public void removeCardByDescription(String cardDescription) {

        Promotion promotion = null;
        Iterator promotionIterator = listOfPromotions.iterator();
        Iterator iterator = listOfAllCards.iterator();
        Card card = null;
        
        while (iterator.hasNext()) {
            card = (Card) iterator.next();
            if (card.GetDescription().equals(cardDescription)) {
                listOfAllCards.remove(card);
                break;
            }
        }

        while(promotionIterator.hasNext()){
            promotion = (Promotion) promotionIterator.next();
            promotion.removeCardByDescription(cardDescription);
        }

    }

    public ArrayList getAvaiableProducts() {
        Iterator iterator1 = listOfPromotions.iterator();
        Iterator iterator2;
        ArrayList<Product> listToReturn = new ArrayList<Product>();
        Promotion promotion;
        while (iterator1.hasNext()) {
            promotion = (Promotion) iterator1.next();
            iterator2 = promotion.getListOfProducts().iterator();
            while (iterator2.hasNext()) {
                listToReturn.add((Product) iterator2.next());
            }
        }

        return listToReturn;

    }

    public Promotion getPromotionByDescription(String description) {
        Iterator iterator = listOfPromotions.iterator();
        Promotion promotion;
        while (iterator.hasNext()) {
            promotion = (Promotion) iterator.next();
            if (promotion.getDescription().equals(description)) {
                return promotion;
            }
        }
        return null;
    }

    public ArrayList<Promotion> getPromotionsList() {
        return listOfPromotions;
    }

    public void addPromotion(Promotion promotion) {
        removePromotionByDescription(promotion.getDescription());
        listOfPromotions.add(promotion);

    }

    public void printAllPromotionsAndCards() {
        Iterator promotionsIterator = listOfPromotions.iterator();
        Iterator cardsIterator = listOfAllCards.iterator();
        Promotion promotion = null;
        Card card = null;
        System.out.println("\nListing all avaiable cards...");
        while (cardsIterator.hasNext()) {
            card = (Card) cardsIterator.next();
            System.out.println("Card: " + card.GetDescription() + " with: " + card.toString());
        }
        System.out.println("Finished printing avaiable cards!\n");

        while (promotionsIterator.hasNext()) {
            promotion = (Promotion) promotionsIterator.next();
            System.out.println("Promotion Description: " + promotion.getDescription());

            cardsIterator = promotion.getCardsList().iterator();
            while (cardsIterator.hasNext()) {
                card = (Card) cardsIterator.next();
                System.out.println("Card: " + card.GetDescription() + " with: " + card.toString());
            }
        }



    }

    public void removePromotionByDescription(String promotionDescription) {
        Iterator iterator = listOfPromotions.iterator();
        Promotion promotion;
        while (iterator.hasNext()) {
            promotion = (Promotion) iterator.next();
            if (promotion.getDescription().equals(promotionDescription)) {
                System.out.println("Removing promotion: " + promotionDescription);
                listOfPromotions.remove(promotion);
                return;
            }
        }
    }

    public void setStore(Store store) {
        this.store = store;
        writePromotionsListToFile();
    }

    public Store getStore() {
        return store;
    }

    public void writePromotionsListToFile() {

        System.out.println("Writting promotions to file...");
        printAllPromotionsAndCards();

        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream(fileName));
            out.writeObject(this);
            out.flush();
            out.close();
        } catch (IOException ex) {
            System.out.println("Error writting promotions list to file: " + fileName + " Error: " + ex);
            return;
        }
        System.out.println("Promotions list written to file: " + fileName);
    }

    public PromotionsList readPromotionsListFromFile() {
        PromotionsList promotionsList = null;
        ObjectInputStream in = null;
        try {
            in = new ObjectInputStream(new FileInputStream(fileName));
            promotionsList = (PromotionsList) in.readObject();
            in.close();
        } catch (Exception ex) {
            System.out.println("Error reading promotions list to file: " + fileName + " Error: " + ex);
            return new PromotionsList();
        }

        return promotionsList;

    }
}
