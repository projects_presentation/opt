package VCPackage;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPConnectionsManager extends Thread {

    private int port;
    private ServerSocket server;
    private Socket socket;
    private PromotionsList promotionsList = null;
    private VCJumbo vcJumbo = null;

    public TCPConnectionsManager(int port, VCJumbo vcJumbo) {
        this.port = port;
        this.vcJumbo = vcJumbo;
        this.start();
    }

    @Override
    public synchronized void run() {
        TCPServer tcpServer;
        try {
            server = new ServerSocket(port);
            System.out.println("À espera que os clientes se liguem ao porto: " + port);
            while (true) {
                socket = server.accept();
                promotionsList = vcJumbo.getPromotionsList();
                System.out.println("O cliente com o IP: " + socket.getInetAddress().getHostAddress() + " ligou-se!");
                tcpServer = new TCPServer(socket, promotionsList);
                tcpServer.start();
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
