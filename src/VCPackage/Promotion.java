package VCPackage;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class Promotion implements Serializable {

    private ArrayList<Product> listOfProducts = new ArrayList<Product>();
    private ArrayList<Card> listOfCards = new ArrayList<Card>();
    
    private ArrayList<String> listOfVCMessages = new ArrayList<String>();
    private ArrayList<String> listOfCCMessages = new ArrayList<String>();
    private String description = null;
    private String code = null;
    private Date initialDate = null;
    private Date finalDate = null;
    private int validDays = 0;
    private String discountType = null;
    private double discountUnit = 0;
    private String discountFlag = null;
    private double discountValue = 0;
    private Boolean adicionalDiscount = false;
    private Boolean currentAccount = false;
    private String adicionalDiscountFlag = null;
    private double adicionalDiscountValue = 0;

    public Promotion(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList getListOfCCMessages() {
        return listOfCCMessages;
    }

    public ArrayList getListOfVCMessages() {
        return listOfVCMessages;
    }

    public void addCCMessage(String message) {
        listOfCCMessages.add(message);
    }

    public void addVCMessage(String message) {
        listOfVCMessages.add(message);
    }

    public Boolean hasProductDefined(int productCode) {
        Product product;
        Iterator iterator = listOfProducts.iterator();
        while (iterator.hasNext()) {
            product = (Product) iterator.next();
            if (product.getType() == productCode) {
                return true;
            }
        }
        return false;
    }

    public Card GetCardByDescription(String cardDescription) {
        Card cardIterated = null;
        Iterator iterator = null;
        iterator = listOfCards.iterator();
        while (iterator.hasNext()) {
            cardIterated = (Card) iterator.next();
            System.out.println("Sending card information from promotion: " + cardIterated.toString());
            if (cardIterated.GetDescription().equals(description)) {
                return cardIterated;
            }
        }
        return null;

    }

    public String getVCMessage(int index){
        try {
            return listOfVCMessages.get(index - 1);
        } catch (Exception e) {
            return "";
        }
    }
    public String getCCMessage(int index){
        try {
            return listOfCCMessages.get(index - 1);
        } catch (Exception e) {
            return "";
        }
    }
    public void AssociateCard(Card card) {
        removeCardByDescription(card.GetDescription());
        listOfCards.add(card);
    }

    public ArrayList<Card> getCardsList() {
        return listOfCards;
    }

    public Card getCardByBIN(String cardBIN) {
        Iterator cardsIterator = listOfCards.iterator();
        Iterator BINsIterator = null;
        Card cardIterated = null;
        if(cardIterated != null) return cardIterated;
        String BINItarated;
        while (cardsIterator.hasNext()) {
            cardIterated = (Card) cardsIterator.next();
            BINsIterator = cardIterated.listOfBINs.iterator();
            while (BINsIterator.hasNext()) {
                BINItarated = (String) BINsIterator.next();
                if (BINItarated.equals(cardBIN.substring(0,8))) {

                    return cardIterated;
                }
            }
        }
        return null;
    }

    public void removeCardByDescription(String cardDescription) {
        Iterator iterator = listOfCards.iterator();
        Card card;
        while (iterator.hasNext()) {
            card = (Card) iterator.next();
            if (card.GetDescription().equals(cardDescription)) {
                listOfCards.remove(card);
                return;
            }
        }
    }

    public void separateFields(String promotionReg) {
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        int index = 1;
        try {
            /*33333333 23092010 23092010 001 Q 000000,00 P 0000 1*/
            code = promotionReg.substring(index, index += 8);
            initialDate = dateFormat.parse(promotionReg.substring(index, index += 8));
            finalDate = dateFormat.parse(promotionReg.substring(index, index += 8));
            validDays = Integer.parseInt(promotionReg.substring(index, index += 3));
            discountType = promotionReg.substring(index, index += 1);
            discountUnit = Double.parseDouble(promotionReg.substring(index, index += 8)); 
            //discountUnit = Integer.parseInt(promotionReg.substring(index, index += 2));
            discountFlag = promotionReg.substring(index, index += 1);
            discountValue = Double.parseDouble(promotionReg.substring(index, index += 4));
            adicionalDiscount = promotionReg.substring(index, index += 1).equals("1") ? true : false;
        } catch (Exception ex) {
            System.out.println("Error separating fields of promotion ERROR: " + ex);
        }
    }

    public void addProduct(Product product) {
        listOfProducts.add(product);
    }

    public void addReceiptMessage(String message) {
        System.out.println("Adding the receipt message: " + message + " to promotion: " + description);
        listOfVCMessages.add(message);
    }

    public void addCurrentAccountMessage(String message) {
        System.out.println("Adding the Account message: " + message + " to promotion: " + description);
        listOfCCMessages.add(message);
    }

    public Boolean getAdicionalDiscount() {
        return adicionalDiscount;
    }

    public String getCode() {
        return code;
    }

    public Boolean getCurrentAccount() {
        return currentAccount;
    }

    public String getDiscountFlag() {
        return discountFlag;
    }

    public String getDiscountType() {
        return discountType;
    }

    public double getDiscountUnit() {
        return discountUnit/100;
    }

    public double getDiscountValue() {
        return discountValue / 100;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public Date getInitialDate() {
        return initialDate;
    }

    public ArrayList<Product> getListOfProducts() {
        return listOfProducts;
    }

    public ArrayList<String> getListOfReceiptMessages() {
        return listOfVCMessages;
    }

    public int getValidDays() {
        return validDays;
    }

    public String getAdicionalDiscountFlag() {
        return adicionalDiscountFlag;
    }

    public double getAdicionalDiscountFlagValue() {
        return adicionalDiscountValue;
    }

    @Override
    public String toString() {
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String actualAdicionalDiscount = adicionalDiscount == true ? "1" : "0";
        String actualCurrentAccount = currentAccount == true ? "1" : "0";

        return "P"
                + code
                + dateFormat.format(initialDate)
                + dateFormat.format(finalDate)
                + new DecimalFormat("000").format(validDays)
                + discountType
                + new DecimalFormat("00000000").format(discountUnit)
                + discountFlag
                + new DecimalFormat("0000").format(discountValue)
                + actualAdicionalDiscount
                + actualCurrentAccount
                + adicionalDiscountFlag
                + new DecimalFormat("0000").format(adicionalDiscountValue);


    }
}
