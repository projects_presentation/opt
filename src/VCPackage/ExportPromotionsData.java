package VCPackage;

import DatabasePackage.MySQLConnection;
import RegistryManager.RegistryManager;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ExportPromotionsData extends Thread {

    // <editor-fold defaultstate="collapsed" desc="Variables">
    private final int EXPORT_HOUR = 23;
    private Boolean export = false;
    private int storeNumber = 0;
    private String date = "";
    private final String REGISTRY_ROOT = "SOFTWARE\\GWORKS\\OPT\\";
    private RegistryManager registryManager = new RegistryManager(REGISTRY_ROOT);
    private String SIACIP = registryManager.readValue("Settings", "SIAC IP", false);
    private int totalOfFillings = 0;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Contructor">
    public ExportPromotionsData(int storeNumber, String date) {
        this.storeNumber = storeNumber;
        this.date = date;
        //start();
    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Export Functions">
    public void CreateFolder(String folderName) {

        System.out.println("Creating folder: " + folderName);
        File f = new File(folderName);
        try {
            if (f.mkdir()) {
                System.out.println("Directory Created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception ex) {
            System.out.println("Error creating folder: " + ex.getMessage());
        }
    }

    public void exportAllData() {

        NumberFormat numberFormat = new DecimalFormat("000");
        Calendar actualDate = new DataHandler().getDate();
        if (actualDate == null) {
            System.out.println("Error getting actual date...");
            return;
        }

        //CreateFolder("\\" + "\\" + SIACIP + "\\SIAC\\PPADRAO\\FLASH\\FC" + date);

        String filePath = "\\" + "\\" + SIACIP + "\\SIAC\\PPADRAO\\FLASH\\FC" + date + "\\" + numberFormat.format(storeNumber) + "IGAS03" + date + ".opt";
        System.out.println("Full File Path: " + filePath);
        SendAllPromotionsFromDatabase(filePath, date);

    }

    public String Header(ResultSet resultSet) {

        String day = date.substring(0, 2);
        String month = date.substring(2, 4);
        String year = "20" + date.substring(4, 6);

        String newDate = year + month + day;

        String stringToReturn = "H";
        NumberFormat numberFormat = new DecimalFormat("000");
        if (storeNumber == 0) {
            return stringToReturn + "000" + newDate;
        } else {
            stringToReturn += numberFormat.format(storeNumber);
        }
        stringToReturn += newDate;
        return stringToReturn;

    }

    public String Footer(int numberOfRegists, int total) {

        NumberFormat numberFormat = new DecimalFormat("000000");
        NumberFormat numberFormat2 = new DecimalFormat("00000000");
        String stringToReturn = "F";
        stringToReturn += numberFormat.format(numberOfRegists);
        stringToReturn += numberFormat2.format(total);
        return stringToReturn;
    }

    public void ExportAllTickets() {

        NumberFormat numberFormat2 = new DecimalFormat("00");
        Calendar actualDate = new DataHandler().getDate();
        SimpleDateFormat dateFormater = new SimpleDateFormat("ddMMyy");
        int optNumber = Integer.parseInt(registryManager.readValue("Settings", "OPT Number", false));
        if (actualDate == null) {
            System.out.println("Error getting actual date...");
            return;
        }

        //CreateFolder("\\" + "\\" + SIACIP + "\\SIAC\\MOVDIA\\MV" + dateFormater.format(actualDate.getTime()));

        String filePath = "\\" + "\\" + SIACIP + "\\SIAC\\MOVDIA\\MV" + dateFormater.format(actualDate.getTime())
                + "\\OPTIC9" + numberFormat2.format(optNumber) + ".PDV";
        System.out.println("Full File Path: " + filePath);
        SendAllTicketsFromDatabase(filePath, date);

    }

    private void SendAllTicketsFromDatabase(String filePath, String date) {

        String day = date.substring(0, 2);
        String month = date.substring(2, 4);
        String year = "20" + date.substring(4, 6);
        String sqlQuery = "SELECT * FROM discount_tickets WHERE YEAR(TIME_AND_DATE) = '" + year + "' AND MONTH(TIME_AND_DATE) = '" + month + "' AND DAY(TIME_AND_DATE) = '" + day + "'";

        ResultSet resultSet = null;
        BufferedWriter out = null;
        MySQLConnection sqlConnection = new MySQLConnection("root", "admin");
        String serverName = registryManager.readValue("Database Connection", "Server Name", false);
        String databaseName = registryManager.readValue("Database Connection", "Database Name", false);
        Statement statement;

        System.out.println("Sending all promotions to: " + filePath);

        try {
            out = new BufferedWriter(new FileWriter(filePath));

        } catch (IOException ex) {
            System.out.println("Error creating buffer: " + ex.getMessage());
        }


        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            resultSet = (ResultSet) statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                out.write("\n\n\n*******************************************************************\n\n\n");
                out.write(resultSet.getString("RECEIPT") + "\n");
                out.write("\n\n\n*******************************************************************\n\n\n");
                System.out.println("Writting to file: " + resultSet.getString("RECEIPT"));
            }
        } catch (Exception ex) {
            System.out.println("Error writting to file: " + ex.getMessage());
        }
        try {
            out.close();
        } catch (IOException ex) {
        }
    }

    private void SendAllPromotionsFromDatabase(String filePath, String date) {

        int numberOfRegists = 0;
        String aux = "";
        /*SELECT * FROM easypay.promotions WHERE YEAR(PROMOTION_TIME_AND_DATE) = '2010' AND MONTH(PROMOTION_TIME_AND_DATE) = '10' AND DAY(PROMOTION_TIME_AND_DATE) = '19';*/
        String day = date.substring(0, 2);
        String month = date.substring(2, 4);
        String year = "20" + date.substring(4, 6);
        String sqlQuery = "SELECT * FROM promotions WHERE YEAR(PROMOTION_TIME_AND_DATE) = '" + year + "' AND MONTH(PROMOTION_TIME_AND_DATE) = '" + month + "' AND DAY(PROMOTION_TIME_AND_DATE) = '" + day + "'";

        ResultSet resultSet = null;
        BufferedWriter out = null;
        MySQLConnection sqlConnection = new MySQLConnection("root", "admin");
        String serverName = registryManager.readValue("Database Connection", "Server Name", false);
        String databaseName = registryManager.readValue("Database Connection", "Database Name", false);
        Statement statement;

        System.out.println("Sending all promotions to: " + filePath);

        try {
            out = new BufferedWriter(new FileWriter(filePath));
        } catch (IOException ex) {
            System.out.println("Error creating buffer: " + ex.getMessage());
        }


        try {
            sqlConnection.connect(serverName, databaseName);
            statement = (Statement) sqlConnection.getConnection().createStatement();
            resultSet = (ResultSet) statement.executeQuery(sqlQuery);

            System.out.println("Writting to file: " + Header(resultSet));
            out.write(Header(resultSet) + "\n");
            totalOfFillings = 0;
            while (resultSet.next()) {
                aux = FormatStringToSendPromotion(resultSet);

                out.write(aux + "\n");
                System.out.println("Writting to file: " + aux);
                ++numberOfRegists;
            }
            System.out.println("Writting to file: " + Footer(numberOfRegists, totalOfFillings));
            out.write(Footer(numberOfRegists, totalOfFillings) + "\n");
        } catch (Exception ex) {
            System.out.println("Error writting to file: " + ex.getMessage());
        }
        try {
            out.close();
        } catch (IOException ex) {
        }
    }

    private String FormatStringToSendPromotion(ResultSet resultSet) {
        String stringToReturn = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyHHmm");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("ddMMyy");
        NumberFormat numberFormat = new DecimalFormat("000");
        NumberFormat numberFormat2 = new DecimalFormat("000000");
        Calendar promotionTimeAndDate = Calendar.getInstance();

        try {
            if (resultSet.getString("PROMOTION_VALUE").equals("000000") && resultSet.getString("ADICIONAL_DISCOUNT").equals("000000")) {
                /*Conteudo layout sem direito a nenhuma promoção*/
                try {
                    promotionTimeAndDate.setTime(resultSet.getTimestamp("PROMOTION_TIME_AND_DATE"));
                    stringToReturn += "P";
                    stringToReturn += resultSet.getString("EAN_CODE");
                    stringToReturn += padLeft(resultSet.getString("PAN"),20);
                    stringToReturn += numberFormat.format(Integer.parseInt(resultSet.getString("STORE_NUMBER")));
                    stringToReturn += resultSet.getString("PROMOTION_CODE");
                    stringToReturn += dateFormat.format(promotionTimeAndDate.getTime());
                    stringToReturn += numberFormat.format(Integer.parseInt(resultSet.getString("OPT_NUMBER")));
                    stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("SEQUENCE_NUMBER")));
                    stringToReturn += "999999";
                    if (resultSet.getString("PRODUCT_CODE").equals("2")) {
                        stringToReturn += "5";
                    } else if (resultSet.getString("PRODUCT_CODE").equals("5")) {
                        stringToReturn += "8";
                    } else {
                        stringToReturn += resultSet.getString("PRODUCT_CODE");
                    }
                    stringToReturn += padRight(resultSet.getString("PRODUCT_DESCRIPTION"), 20);
                    stringToReturn += resultSet.getString("VOLUME");
                    stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("FILLING_VALUE")));
                    stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("PROMOTION_VALUE")));
                    stringToReturn += "00000000000000";
                    stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("ADICIONAL_DISCOUNT")));
                    stringToReturn += resultSet.getString("NO_RECEIPT");

                } catch (SQLException ex) {
                    System.out.println("Error exporting promotions: " + ex);
                    return "P00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
                }
                totalOfFillings += Integer.parseInt(resultSet.getString("FILLING_VALUE"));
                System.out.println("Total de abastecimentos: " + totalOfFillings);
                return stringToReturn;
            } else if (resultSet.getString("TYPE_OF_DISCOUNT").equals("V")) {
                /*Conteudo layout para vale de compras...*/
                try {
                    promotionTimeAndDate.setTime(resultSet.getTimestamp("PROMOTION_TIME_AND_DATE"));
                    stringToReturn += resultSet.getString("TYPE_OF_DISCOUNT");
                    stringToReturn += resultSet.getString("EAN_CODE");
                    stringToReturn += padLeft(resultSet.getString("PAN"),20);
                    stringToReturn += numberFormat.format(Integer.parseInt(resultSet.getString("STORE_NUMBER")));
                    stringToReturn += resultSet.getString("PROMOTION_CODE");
                    stringToReturn += dateFormat.format(promotionTimeAndDate.getTime());
                    stringToReturn += numberFormat.format(Integer.parseInt(resultSet.getString("OPT_NUMBER")));
                    stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("SEQUENCE_NUMBER")));
                    stringToReturn += "999999";
                    if (resultSet.getString("PRODUCT_CODE").equals("2")) {
                        stringToReturn += "5";
                    } else if (resultSet.getString("PRODUCT_CODE").equals("5")) {
                        stringToReturn += "8";
                    } else {
                        stringToReturn += resultSet.getString("PRODUCT_CODE");
                    }
                    stringToReturn += padRight(resultSet.getString("PRODUCT_DESCRIPTION"), 20);
                    stringToReturn += resultSet.getString("VOLUME");
                    stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("FILLING_VALUE")));
                    stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("PROMOTION_VALUE")));
                    stringToReturn += dateFormat2.format(resultSet.getDate("START_DATE"));
                    stringToReturn += dateFormat2.format(resultSet.getDate("END_DATE"));
                    stringToReturn += "00";
                    stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("ADICIONAL_DISCOUNT")));
                    stringToReturn += resultSet.getString("NO_RECEIPT");

                } catch (SQLException ex) {
                    System.out.println("Error exporting promotions: " + ex);
                    return "V00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
                }
                totalOfFillings += Integer.parseInt(resultSet.getString("FILLING_VALUE"));
                System.out.println("Total de abastecimentos: " + totalOfFillings);
                return stringToReturn;
            } else if (resultSet.getString("TYPE_OF_DISCOUNT").equals("C")) {
                /*Conteudo layout para conta corrente...*/
                try {
                    promotionTimeAndDate.setTime(resultSet.getTimestamp("PROMOTION_TIME_AND_DATE"));
                    stringToReturn += resultSet.getString("TYPE_OF_DISCOUNT");
                    stringToReturn += resultSet.getString("EAN_CODE");
                    stringToReturn += padLeft(resultSet.getString("PAN"),20);
                    stringToReturn += numberFormat.format(Integer.parseInt(resultSet.getString("STORE_NUMBER")));
                    stringToReturn += resultSet.getString("PROMOTION_CODE");
                    stringToReturn += dateFormat.format(promotionTimeAndDate.getTime());
                    stringToReturn += numberFormat.format(Integer.parseInt(resultSet.getString("OPT_NUMBER")));
                    stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("SEQUENCE_NUMBER")));
                    stringToReturn += "999999";
                    if (resultSet.getString("PRODUCT_CODE").equals("2")) {
                        stringToReturn += "5";
                    } else if (resultSet.getString("PRODUCT_CODE").equals("5")) {
                        stringToReturn += "8";
                    } else {
                        stringToReturn += resultSet.getString("PRODUCT_CODE");
                    }
                    stringToReturn += padRight(resultSet.getString("PRODUCT_DESCRIPTION"), 20);
                    stringToReturn += resultSet.getString("VOLUME");
                    stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("FILLING_VALUE")));
                    stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("PROMOTION_VALUE")));
                    stringToReturn += dateFormat2.format(resultSet.getDate("START_DATE"));
                    stringToReturn += dateFormat2.format(resultSet.getDate("END_DATE"));
                    stringToReturn += "00";                    
                    stringToReturn += numberFormat2.format(Integer.parseInt(resultSet.getString("ADICIONAL_DISCOUNT")));
                    stringToReturn += resultSet.getString("NO_RECEIPT");
                } catch (SQLException ex) {
                    System.out.println("Error exporting promotions: " + ex);
                    return "C00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
                }

                totalOfFillings += Integer.parseInt(resultSet.getString("FILLING_VALUE"));
                System.out.println("Total de abastecimentos: " + totalOfFillings);


                return stringToReturn;
            }
        } catch (SQLException ex) {
            return "E00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
        }

        return "A00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    }

    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }
    
    public static String padLeft(String s, int n) {
        String stringToReturn = s.replaceAll(" ","");
        return String.format("%1$" + n + "s", stringToReturn).replace(" ","0");
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Run Method">
    @Override
    public void run() {
        Calendar actualTime = null;
        while (true) {
            actualTime = Calendar.getInstance();
            if (actualTime.get(Calendar.HOUR_OF_DAY) == EXPORT_HOUR - 1) {
                export = true;
            }
            if (actualTime.get(Calendar.HOUR_OF_DAY) == EXPORT_HOUR) {
                if (export) {
                    exportAllData();
                    export = false;
                }
            }
            try {
                Thread.sleep(100);
            } catch (Exception ex) {
            }
        }
    }// </editor-fold>
}
