package VCPackage;

import java.io.Serializable;

public class Store implements Serializable{

    private int number = 0;
    private String name = null;

    public Store(String storeReg){
        separateFields(storeReg);
    }

    public void separateFields(String storeReg){
        int index = 1;
        number = Integer.parseInt(storeReg.substring(index, index+=3));
        name = storeReg.substring(index, index+=30);

    }

    public String getName() {
        return name.trim();
    }
    public int getNumber() {
        return number;
    }

    @Override
    public String toString(){
        return number+name;
    }
  
}
