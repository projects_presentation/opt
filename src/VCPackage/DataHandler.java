package VCPackage;

import FileLogger.FileLogger;
import RegistryManager.RegistryManager;
import TPAPackage.TCPClient;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Calendar;

public class DataHandler {

    private final String REGISTRY_ROOT = "SOFTWARE\\GWORKS\\OPT\\";
    private RegistryManager registryManager = new RegistryManager(REGISTRY_ROOT);
    private String SIACIP = "";
    private String filePath = "";
    private FileLogger logger;

    public DataHandler() {

        SIACIP = registryManager.readValue("Settings", "SIAC IP", false);
        filePath = "\\" + "\\" + SIACIP + "\\SIAC\\MOVDIA\\DATA.PDV";
        System.out.println("Full File Path: " + filePath);
    }

    public Calendar getDate() {

        FileInputStream fstream = null;
        DataInputStream dataInputStream = null;
        BufferedReader bufferedReader = null;
        String strLine;
        Calendar calendar = Calendar.getInstance();

        try {
            fstream = new FileInputStream(filePath);
            dataInputStream = new DataInputStream(fstream);
            bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));
            while ((strLine = bufferedReader.readLine()) != null) {
                if (strLine.length() != 6) {
                    System.out.println("\r\nErro ficheiro DATA.PDV mal formatado...");
                    logger.logString("\r\nErro ficheiro DATA.PDV mal formatado...", logger.ERROR);
                } else {
                    calendar.set(Integer.parseInt(strLine.substring(4, 6)) + 2000, Integer.parseInt(strLine.substring(2, 4)) - 1, Integer.parseInt(strLine.substring(0, 2)));
                    registryManager.setValue("Settings", "LastReadedData",strLine, false);
                    fstream.close();
                    dataInputStream.close();
                    bufferedReader.close();                    
                    return calendar;
                }
            }
            fstream.close();
            dataInputStream.close();
            bufferedReader.close();
        } catch (Exception ex) {
            System.out.println("Error getting date file: " + ex.getMessage());
        }

        String lastReadedData = registryManager.readValue("Settings", "LastReadedData", false);
        if (lastReadedData.equals("")) {
            return Calendar.getInstance();
        } else {
            calendar.set(Integer.parseInt(lastReadedData.substring(4, 6)) + 2000, Integer.parseInt(lastReadedData.substring(2, 4)) - 1, Integer.parseInt(lastReadedData.substring(0, 2)));
            System.out.println("Error getting data from SIAC returning: " + lastReadedData);
            return calendar;
        }
    }
}
