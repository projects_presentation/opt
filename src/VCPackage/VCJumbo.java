package VCPackage;

import DatabasePackage.MySQLConnection;
import RegistryManager.RegistryManager;
import TPAPackage.SecurityModule;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

public class VCJumbo implements Serializable {

    private static final String REGISTRY_ROOT = "SOFTWARE\\GWorks\\OPT\\";
    RegistryManager registryManager = new RegistryManager(REGISTRY_ROOT);
    private PromotionsList promotionsList = new PromotionsList();
    byte[] logo1 = {0x1C, 0x70, 0x01, 0x30};
    byte[] increase = {0x1B, 0x0E};
    byte[] decrease = {0x1B, 0x14};

    public VCJumbo() {
        updatePromotionsList();
        new TCPConnectionsManager(8000, this);
    }

    public PromotionsList getPromotionsList() {
        updatePromotionsList();
        return promotionsList;
    }
    /*
    private void testPromotions() {
    promotionsList = promotionsList.readPromotionsListFromFile();
    if (promotionsList != null) {
    System.out.println("There are already " + promotionsList.getPromotionsList().size() + " promotions defined");
    } else {

    System.out.println("There are no promotions defined");
    promotionsList = new PromotionsList();

    ///////////////////////////////////////////////////////
    Promotion promotion = new Promotion("Promocao 1");
    promotion.separateFields("Pcccccccc0110201030102010030Q10V010011");

    promotion.addProduct(new Product("A" + promotion.getCode() + "02EEEEEEEEEEEEE"));
    promotion.addProduct(new Product("A" + promotion.getCode() + "03FFFFFFFFFFFFF"));

    promotion.addVCMessage("M1WWWWWWWWWWWWWWWWWWWWWWWWWWWW1");
    promotion.addVCMessage("M2WWWWWWWWWWWWWWWWWWWWWWWWWWWW2");
    promotion.addCCMessage("C1XXXXXXXXXXXXXXXXXXXXXXXXXXXX1");
    promotion.addCCMessage("C2XXXXXXXXXXXXXXXXXXXXXXXXXXXX2");

    ///////////////////////////////////////////////////////
    Promotion promotion2 = new Promotion("Promocao 2");
    promotion2.separateFields("Pbbbbbbbb0110201030102010030V10P010000");

    promotion2.addProduct(new Product("A" + promotion2.getCode() + "05EEEEEEEEEEEEE"));
    promotion2.addVCMessage("M1AAAAAAAAAAAAAAAAAAAAAAAAAAAA1");
    promotion2.addVCMessage("M2BBBBBBBBBBBBBBBBBBBBBBBBBBBB2");
    promotion2.addCCMessage("C1CCCCCCCCCCCCCCCCCCCCCCCCCCCC1");
    promotion2.addCCMessage("C2DDDDDDDDDDDDDDDDDDDDDDDDDDDD2");

    ///////////////////////////////////////////////////////
    Store store = new Store("L123GASOLINEIRA DA LISBOA         ");
    ///////////////////////////////////////////////////////
    Card card1 = new Card("Jumbo Mais");
    card1.separateFields("TV0100C");
    card1.AddBIN("45447500");
    card1.AddBIN("67081207");
    card1.AddBIN("22222222");
    card1.AddBIN("33333333");

    ///////////////////////////////////////////////////////
    Card card2 = new Card("Jumbo Norauto");
    card2.separateFields("TP0010V");
    card2.AddBIN("44444444");
    card2.AddBIN("55555555");
    card2.AddBIN("66666666");
    card2.AddBIN("77777777");
    ///////////////////////////////////////////////////////
    promotion.AssociateCard(card1);
    promotion.AssociateCard(card2);

    promotionsList.addCard(card1);
    promotionsList.addCard(card2);

    promotionsList.setStore(store);
    promotionsList.addPromotion(promotion);
    promotionsList.addPromotion(promotion2);
    ///////////////////////////////////////////////////////
    promotionsList.writePromotionsListToFile();

    }
    }
     */

    public void setPromotions(PromotionsList promotionsList) {
        this.promotionsList = promotionsList;
    }

    public void updatePromotionsList() {
        if (promotionsList == null) {
            promotionsList = new PromotionsList();
        }
        promotionsList = promotionsList.readPromotionsListFromFile();
        if (promotionsList != null) {
            System.out.println("Promotions....");
        } else {
            System.out.println("Error there are no promotions avaiable!!!");
        }
    }

    public Promotion applyPromotion(int productCode) {
        Promotion promotion = null;
        Iterator iterator = promotionsList.getPromotionsList().iterator();
        while (iterator.hasNext()) {
            promotion = (Promotion) iterator.next();
            if (promotion.hasProductDefined(productCode) && promotionIsInsideDate(promotion)) {
                return promotion;
            }
        }
        return null;
    }

    public Boolean promotionIsInsideDate(Promotion promotion) {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar actualDate = null;//new DataHandler().getDate();

        if (actualDate == null) {
            actualDate = Calendar.getInstance();
        }


        System.out.println("Data actual: " + dateFormat.format(actualDate.getTime()));
        System.out.println("Data de inicio da promocao: " + dateFormat.format(promotion.getInitialDate()));
        System.out.println("Data final da promocao: " + dateFormat.format(promotion.getFinalDate()));

        if (actualDate.getTime().after(promotion.getInitialDate()) && actualDate.getTime().before(promotion.getFinalDate())) {
            return true;
        } else if (datesAreEqual(actualDate.getTime(), promotion.getInitialDate()) || datesAreEqual(actualDate.getTime(), promotion.getFinalDate())) {
            return true;
        } else {
            System.out.println("Erro promocao fora de prazo...!");
            String dateOutOfTime1 = actualDate.getTime().after(promotion.getInitialDate()) ? "Data actual é depois da data inicial" : "Data actual é antes da data inicial";
            String dateOutOfTime2 = actualDate.getTime().before(promotion.getFinalDate()) ? "Data actual é antes da data final" : "Data actual é depois da data final";
            System.out.println(dateOutOfTime1);
            System.out.println(dateOutOfTime2);
            return false;
        }

    }

    private Boolean datesAreEqual(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(date1);
        calendar2.setTime(date2);
        if (calendar1.get(Calendar.DAY_OF_WEEK) == calendar2.get(Calendar.DAY_OF_WEEK)
                && calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)
                && calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)) {
            return true;
        }
        return false;

    }

    public String composeReceiptToPrintForCard(Double valueOfPromotion, Promotion promotion, int sequenceNumber, String BIN, int transactionNumber) {

        NumberFormat promoFormat = new DecimalFormat("0.00");
        Card card = promotion.getCardByBIN(BIN);
        if (card == null) {
            System.out.println("O cartao nao existe!!!");
            return "";
        }

        Store store = promotionsList.getStore();
        if (store == null) {
            store = new Store("L000SEM LOJA DEFINIDA             ");
        }

        String promotionType = card.getAccountDiscount().equals("C") ? "Conta Corrente" : "Vale de Compras";

        String value = promoFormat.format(valueOfPromotion);
        value = value.toString().replaceAll("\\.", ",");

        Calendar actualDate = Calendar.getInstance();//new DataHandler().getDate();
        /*
        actualDate.set(Calendar.YEAR, actualDate.get(Calendar.YEAR) + 2000);

        if (actualDate.get(Calendar.YEAR) > 3000) {
            actualDate.set(Calendar.YEAR, actualDate.get(Calendar.YEAR) - 2000);
        }
        */


        String dateToReceipt = new SimpleDateFormat("dd/MM/yyyy").format(actualDate.getTime()).substring(0, 10).replaceAll("/", "-");
        NumberFormat sequenceNumberFormater = new DecimalFormat("000000");
        String receiptComposed = "";

        receiptComposed += "\n";
        receiptComposed += "Desconto em cartao Jumbo";
        receiptComposed += "\n";
        receiptComposed += "\n";
        receiptComposed += "Emissor:" + store.getName() + "\n";
        receiptComposed += "\n";
        receiptComposed += new String(increase) + "Valor de:" + new String(decrease) + "\n";
        receiptComposed += new String(increase) + value + " EUR" + new String(decrease) + "\n";
        receiptComposed += "\n";
        receiptComposed += "Desconto em " + promotionType;
        receiptComposed += "\n";
        receiptComposed += "Para o cartão " + BIN + "****";
        receiptComposed += "\n";
        receiptComposed += getDateOfPromo(promotion) + "\n";
        receiptComposed += "\n";
        //receiptComposed += "----------------------\n";
        receiptComposed += "\n";
        receiptComposed += getBarCode();
        receiptComposed += "\n";

        receiptComposed += promotion.getCCMessage(1).substring(1) + "\n";
        receiptComposed += promotion.getCCMessage(2).substring(1) + "\n";
        receiptComposed += promotion.getCCMessage(3).substring(1) + "\n";
        //receiptComposed += "----------------------\n";
        receiptComposed += dateToReceipt + "     " + sequenceNumberFormater.format(sequenceNumber);
        receiptComposed += "\n";
        receiptComposed += "\n";
        receiptComposed += "Vale de compras \n referente ao talao \n N: " + transactionNumber;

        return receiptComposed;
    }

    public String composeReceiptToPrint(double valueOfPromotion, double adicionalValue, Promotion promotion, int sequenceNumber, String PAN, int transactionNumber) {

        NumberFormat promoFormat = new DecimalFormat("0.00");
        Store store = promotionsList.getStore();
        if (store == null) {
            store = new Store("L000SEM LOJA DEFINIDA             ");
        }
        Calendar actualDate = new DataHandler().getDate();
        actualDate.set(Calendar.YEAR, actualDate.get(Calendar.YEAR) + 2000);
        if (actualDate.get(Calendar.YEAR) > 3000) {
            actualDate.set(Calendar.YEAR, actualDate.get(Calendar.YEAR) - 2000);
        }
        String value = promoFormat.format(valueOfPromotion + adicionalValue);
        value = value.toString().replaceAll("\\.", ",");
        String dateToReceipt = new SimpleDateFormat("dd/MM/yyyy").format(actualDate.getTime()).substring(0, 10).replaceAll("/", "-");
        NumberFormat sequenceNumberFormater = new DecimalFormat("000000");
        String receiptComposed = "";
        //receiptComposed += "\n";
        receiptComposed += "Emissor:" + store.getName() + "\n";
        //receiptComposed += "\n";
        receiptComposed += new String(increase) + "Valor de:" + new String(decrease) + "\n";
        receiptComposed += new String(increase) + value + " EUR" + new String(decrease) + "\n";
        //receiptComposed += "\n";
        receiptComposed += getDateOfPromo(promotion) + "\n";
        //receiptComposed += "\n";
        //receiptComposed += "----------------------\n";
        //receiptComposed += "\n";
        receiptComposed += getBarCode();
        //receiptComposed += "\n";

        receiptComposed += promotion.getVCMessage(1).substring(1) + "\n";
        receiptComposed += promotion.getVCMessage(2).substring(1) + "\n";
        receiptComposed += promotion.getVCMessage(3).substring(1) + "\n";
        //receiptComposed += "----------------------\n";
        receiptComposed += dateToReceipt + "     " + sequenceNumberFormater.format(sequenceNumber);
        //receiptComposed += "\n";
        //receiptComposed += "\n";
        receiptComposed += "\nVale de compras \n referente ao talao \n N: " + transactionNumber;



        return receiptComposed;
    }

    public String getDateOfPromo(Promotion promotion) {

        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy");
        Calendar calendar = new DataHandler().getDate();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        String initialDate = sdf.format(calendar.getTime());
        calendar.add(Calendar.DAY_OF_MONTH, promotion.getValidDays() - 1);
        String finalDate = sdf.format(calendar.getTime());
        initialDate = initialDate.substring(0, 2) + "/" + initialDate.substring(2, 4) + "/" + initialDate.substring(4, 6);
        finalDate = finalDate.substring(0, 2) + "/" + finalDate.substring(2, 4) + "/" + finalDate.substring(4, 6);
        return "Valido de " + initialDate + " a " + finalDate;
    }

    public String getBarCode() {

        byte[] placeOfChars = {0x1D, 0x48, 0x32};
        byte[] font = {0x1D, 0x66, 0x30};
        byte[] size = {0x1D, 0x68, 0x64};
        int optNumber = Integer.parseInt(registryManager.readValue("Settings", "OPT Number", false));
        Store store = promotionsList.getStore();
        if (store == null) {
            store = new Store("L000SEM LOJA DEFINIDA             ");
        }
        byte[] printCodeBar = {0x1D, 0x6B, 0x43, 0x0D};
        String barCodeFormated = "";
        NumberFormat sequenceNumberFormater = new DecimalFormat("000000");
        NumberFormat optNumberFormater = new DecimalFormat("000");
        NumberFormat storeNumberFormater = new DecimalFormat("000");
        int sequenceNumber = Integer.parseInt(registryManager.readValue("Settings", "SequenceNumber", false));
        int storeNumber = store.getNumber();
        barCodeFormated = formatBarCode(storeNumberFormater.format(storeNumber), optNumberFormater.format(optNumber), sequenceNumberFormater.format(sequenceNumber));
        return new String(placeOfChars) + new String(font) + new String(size) + new String(printCodeBar) + barCodeFormated;
    }

    private String formatBarCode(String L, String P, String S) {
        String aux = "";
        int i = 1;
        aux += "9";
        aux += "9";
        aux += L.charAt(1 - i);
        aux += P.charAt(3 - i);
        aux += S.charAt(2 - i);
        aux += S.charAt(3 - i);
        aux += L.charAt(2 - i);
        aux += P.charAt(2 - i);
        aux += S.charAt(4 - i);
        aux += S.charAt(5 - i);
        aux += L.charAt(3 - i);
        aux += S.charAt(6 - i);
        aux += calculateControlDigit(aux);
        return aux;
        //    L1 P3 S2 S3 L2 P2 S4 S5 L3 S6 D
        // 99 9  1  0  0  0  0  1  7  1  5  4
        /*
         Regra IX:
            O formato da codificação deverá ser o descrito abaixo:

                A1 A2 L1 P3 S2 S3 L2 P2 S4 S5 L3 S6  D

                Onde:
                A1A2                           - Nº constante   “99”     (específico para este projecto)
                L1L2L3                        - Nº da Loja                  (obtido do ficheiro promopt.NNN – registo L)
                P1 P2P3                       - Nº da OPT                  (P1 fica fora da regra)
                S1 S2S3S4S5S6          - Nº Sequência  (S1 está fora da regra, sequencializador do VC)
                D                                 - Dígito de Controle
         *
            Regra X:

                O código do Vale Compra deverá:

                (a)   seguir o padrão EAN-13
                (b)   usar uma fonte de impressão para o código de barras EAN,
                (c)   ter o valor numérico do código.
         */
    }

    public String calculateControlDigit(String eanCode) {
        int controlDigit = 0;
        if (eanCode.length() != 12) {
            return "";
        }
        for (int i = 0; i < eanCode.length(); ++i) {
            if (i % 2 == 0) {
                controlDigit += Integer.parseInt(eanCode.substring(i, i + 1));
            } else {
                controlDigit += (Integer.parseInt(eanCode.substring(i, i + 1)) * 3);
            }
        }
        if (controlDigit >= 100) {
            controlDigit -= 100;
        }
        controlDigit = controlDigit % 10;
        controlDigit = 10 - controlDigit;
        if (controlDigit == 10) {
            controlDigit = 0;
        }
        return controlDigit + "";
    }

    public double getPromotionValueNormalCustomer(Promotion promotion, double litres, double value) {

        if (promotion == null) {
            return 0.0;
        }
        System.out.println("Promotion discount type: " + promotion.getDiscountType());
        System.out.println("Promotion discount unit: " + promotion.getDiscountUnit());
        System.out.println("Promotion discount flag: " + promotion.getDiscountFlag());
        System.out.println("Promotion discount value: " + promotion.getDiscountValue());

        NumberFormat promoFormat = new DecimalFormat("#,##0,00");
        NumberFormat priceFormat = new DecimalFormat("0.000");
        double valueToReturn = 0;
        double pricePerLitre = value / litres;
        System.out.println("Price per litre: " + priceFormat.format(pricePerLitre));

        try {
            if (promotion.getDiscountType().equals("Q")) {
                if (promotion.getDiscountFlag().equals("V")) {
                    while ((litres >= promotion.getDiscountUnit())) {
                        litres -= promotion.getDiscountUnit();
                        valueToReturn += promotion.getDiscountValue();
                    }
                    System.out.println("Return a discount in value of " + valueToReturn);
                } else if (promotion.getDiscountFlag().equals("P")) {
                    while ((litres >= promotion.getDiscountUnit())) {
                        litres -= promotion.getDiscountUnit();
                        valueToReturn += (promotion.getDiscountUnit() * pricePerLitre) * promotion.getDiscountValue();
                    }
                    valueToReturn = valueToReturn / 100;
                    System.out.println("Return a discount in percentage of " + valueToReturn);
                }
            } else if (promotion.getDiscountType().equals("V")) {
                if (promotion.getDiscountFlag().equals("V")) {
                    while ((value >= promotion.getDiscountUnit())) {
                        value -= promotion.getDiscountUnit();
                        valueToReturn += promotion.getDiscountValue();
                    }
                    System.out.println("Return a discount in value of " + valueToReturn);
                } else if (promotion.getDiscountFlag().equals("P")) {
                    while ((value >= promotion.getDiscountUnit())) {
                        value -= promotion.getDiscountUnit();
                        valueToReturn += (promotion.getDiscountUnit()) * promotion.getDiscountValue();
                    }
                    valueToReturn = valueToReturn / 100;
                    System.out.println("Return a discount in percentage of " + valueToReturn);
                }
            } else {
                System.out.println("Error no such promotion was defined!!!!");
                valueToReturn = Double.parseDouble(promoFormat.format(0));
            }

        } catch (NumberFormatException numberFormatException) {
            System.out.println("Error on promotion: " + numberFormatException.getMessage());
            return 0.0;
        }
        valueToReturn = valueToReturn * 100;
        String aux = promoFormat.format(valueToReturn).replaceAll("\\.", "");
        aux = aux.replaceAll(",", "");
        valueToReturn = Double.parseDouble(aux) / 100;
        return valueToReturn;
    }

    public double getPromotionValueCardCustomer(String PAN, Promotion promotion, double litres, double value) {

        if (promotion == null) {
            return 0.0;
        }
        NumberFormat promoFormat = new DecimalFormat("#,##0,00");
        NumberFormat priceFormat = new DecimalFormat("0.000");
        double valueToReturn = 0;
        double pricePerLitre = value / litres;
        System.out.println("Price per litre: " + priceFormat.format(pricePerLitre));

        Card card = null;

        if (PAN.substring(0, 8).equals("")) {
            return 0.0;
        }
        if (promotion == null) {
            return 0.0;
        } else {
            card = promotion.getCardByBIN(PAN.substring(0, 8));
        }

        if (card == null) {
            System.out.println("O BIN " + PAN.substring(0, 8) + " nao existe para esta promocao");
            return 0.0;
        } else {
            System.out.println("O cartao " + card.GetDescription() + " contem o BIN: " + PAN.substring(0, 8));
            if (promotion.getDiscountType().equals("Q")) {
                if (litres >= promotion.getDiscountUnit()) {
                    if (card.GetDiscountFlag().equals("V")) {
                        while ((litres >= promotion.getDiscountUnit())) {
                            litres -= promotion.getDiscountUnit();
                            valueToReturn += card.getDiscountUnit();
                        }
                        System.out.println("Return a discount in value of " + valueToReturn);
                    } else if (card.GetDiscountFlag().equals("P")) {
                        while ((litres >= promotion.getDiscountUnit())) {
                            litres -= promotion.getDiscountUnit();
                            valueToReturn += (promotion.getDiscountUnit() * pricePerLitre) * card.getDiscountUnit();
                        }
                        valueToReturn = valueToReturn / 100;
                        System.out.println("Return a discount in percentage of " + valueToReturn);
                    } else {
                        return 0.0;
                    }
                }
            } else if (promotion.getDiscountType().equals("V")) {
                if (value >= promotion.getDiscountUnit()) {
                    if (card.GetDiscountFlag().equals("V")) {
                        while ((value >= promotion.getDiscountUnit())) {
                            value -= promotion.getDiscountUnit();
                            valueToReturn += card.getDiscountUnit();
                        }
                        System.out.println("Return a discount in value of " + valueToReturn);
                    } else if (card.GetDiscountFlag().equals("P")) {
                        while ((value >= promotion.getDiscountUnit())) {
                            value -= promotion.getDiscountUnit();
                            valueToReturn += (promotion.getDiscountUnit()) * card.getDiscountUnit();
                        }
                        valueToReturn = valueToReturn / 100;
                        System.out.println("Return a discount in percentage of " + valueToReturn);
                    } else {
                        return 0.0;
                    }
                }
            } else {
                System.out.println("Error no such promotion was defined!!!!");
                valueToReturn = Double.parseDouble(promoFormat.format(0));
            }

            valueToReturn = valueToReturn * 100;
            String aux = promoFormat.format(valueToReturn).replaceAll("\\.", "");
            aux = aux.replaceAll(",", "");
            valueToReturn = Double.parseDouble(aux) / 100;
            return valueToReturn;
        }
    }

    public void updateDatabaseDiscounts(String EANCode, String PAN, Promotion promotion, int transactionNumber, double normalValue, double adicionalValue, int receiptRequested, int fillingValue, int productCode, String productDescription, double volume) {

        if(!PAN.equals("")){
            PAN = PAN.replaceAll("D"," ");
        }
        if(productCode > 10){
            productCode = 0;
        }
        if (promotion == null) {
            promotion = new Promotion("");
            promotion.separateFields("P000000000101200001012000000Q00V000000");
        }
        if (normalValue != 0.0 || adicionalValue != 0.0) {
            InsertDiscountIntoDatabase(EANCode, PAN, promotion, transactionNumber, normalValue, adicionalValue, receiptRequested, fillingValue, productCode, productDescription, volume);
        } else if (normalValue == 0.0 && adicionalValue == 0.0) {
            if (promotionsList.getCardByBIN(PAN.substring(0, 8)) != null) {
                InsertDiscountIntoDatabase(EANCode, PAN, promotion, transactionNumber, normalValue, adicionalValue, receiptRequested, fillingValue, productCode, productDescription, volume);
            }
        }
    }

    public void InsertDiscountIntoDatabase(String EANCode, String PAN, Promotion promotion, int transactionNumber, double normalValue, double adicionalValue, int receiptRequested, int fillingValue, int productCode, String productDescription, double volume) {
        Calendar actualDate = new DataHandler().getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Card card = promotion.getCardByBIN(PAN.substring(0, 8));

        int sequenceNumber = Integer.parseInt(registryManager.readValue("Settings", "SequenceNumber", false));
        registryManager.setValue("Settings", "SequenceNumber", ++sequenceNumber + "", false);
        
        if (card == null) {
            card = new Card("");
            if (normalValue > 0 || adicionalValue > 0) {
                card.separateFields("TV0000V");
            } else {
                card.separateFields("TV0000P");
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy");
        Calendar calendar = new DataHandler().getDate();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        String initialDate = sdf.format(calendar.getTime());
        calendar.add(Calendar.DAY_OF_MONTH, promotion.getValidDays() - 1);
        String finalDate = sdf.format(calendar.getTime());
        initialDate = initialDate.substring(4, 6) + "/" + initialDate.substring(2, 4) + "/" + initialDate.substring(0, 2);
        finalDate = finalDate.substring(4, 6) + "/" + finalDate.substring(2, 4) + "/" + finalDate.substring(0, 2);
        Store store = promotionsList.getStore();
        if (store == null) {
            store = new Store("L000SEM LOJA DEFINIDA             ");
        }

        NumberFormat promoFormat = new DecimalFormat("#,##0.00");
        NumberFormat numberFormat = new DecimalFormat("000000");
        NumberFormat numberFormat2 = new DecimalFormat("00000");

        String optNumber = registryManager.readValue("Settings", "OPT Number", false);
        String actualDateInString = dateFormat.format(actualDate.getTime());
        MySQLConnection sqlConnection = new MySQLConnection("root", "admin");
        String serverName = registryManager.readValue("Database Connection", "Server Name", false);
        String databaseName = registryManager.readValue("Database Connection", "Database Name", false);
        sqlConnection.connect(serverName, databaseName);

        Calendar promotionValidDate = Calendar.getInstance();
        promotionValidDate.setTime(promotion.getInitialDate());
        promotionValidDate.add(Calendar.DAY_OF_MONTH, promotion.getValidDays() - 1);

        String statementToExecute = "INSERT INTO promotions ("
                + "TYPE_OF_DISCOUNT"
                + ",EAN_CODE"
                + ",PAN"
                + ",STORE_NUMBER"
                + ",PROMOTION_CODE"
                + ",PROMOTION_TIME_AND_DATE"
                + ",OPT_NUMBER"
                + ",SEQUENCE_NUMBER"
                + ",PROMOTION_VALUE"
                + ",START_DATE"
                + ",END_DATE"
                + ",ADICIONAL_DISCOUNT"
                + ",NO_RECEIPT"
                + ",FILLING_VALUE"
                + ",PRODUCT_CODE"
                + ",PRODUCT_DESCRIPTION"
                + ",VOLUME"
                + ") VALUES ('"
                + card.getAccountDiscount() + "','"
                + EANCode.substring(13) + "','"
                + PAN + "','"
                + store.getNumber() + "','"
                + promotion.getCode() + "','"
                + actualDateInString + "','"
                + optNumber + "','"
                + transactionNumber + "','"
                + numberFormat.format(Integer.parseInt(promoFormat.format(normalValue).replaceAll(",", ""))) + "','"
                + initialDate + "','"
                + finalDate + "','"
                + numberFormat.format(Integer.parseInt(promoFormat.format(adicionalValue).replaceAll(",", ""))) + "','"
                + receiptRequested + "','"
                + fillingValue + "','"
                + productCode + "','"
                + productDescription + "','"
                + numberFormat2.format(Integer.parseInt(promoFormat.format(volume).replaceAll(",", ""))) + "')";
        System.out.println("Executing statment: " + statementToExecute);
        sqlConnection.executeStatement(statementToExecute);
        sqlConnection.disconnect();
    }
    
   /* public void InsertDiscountWithoutPromotion(String PAN ,int transactionNumber,int receiptRequested, int fillingValue, int productCode, String productDescription, double volume){
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy");
        String actualDate = sdf.format(Calendar.getInstance());
        
        int sequenceNumber = Integer.parseInt(registryManager.readValue("Settings", "SequenceNumber", false));
        registryManager.setValue("Settings", "SequenceNumber", ++sequenceNumber + "", false);
        Store store = promotionsList.getStore();
        if (store == null) {
            store = new Store("L000SEM LOJA DEFINIDA             ");
        }

        NumberFormat promoFormat = new DecimalFormat("#,##0.00");
        NumberFormat numberFormat2 = new DecimalFormat("00000");

        String optNumber = registryManager.readValue("Settings", "OPT Number", false);
        String actualDateInString = dateFormat.format(Calendar.getInstance());
        MySQLConnection sqlConnection = new MySQLConnection("root", "admin");
        String serverName = registryManager.readValue("Database Connection", "Server Name", false);
        String databaseName = registryManager.readValue("Database Connection", "Database Name", false);
        sqlConnection.connect(serverName, databaseName);

        String statementToExecute = "INSERT INTO promotions ("
                + "TYPE_OF_DISCOUNT"
                + ",EAN_CODE"
                + ",PAN"
                + ",STORE_NUMBER"
                + ",PROMOTION_CODE"
                + ",PROMOTION_TIME_AND_DATE"
                + ",OPT_NUMBER"
                + ",SEQUENCE_NUMBER"
                + ",PROMOTION_VALUE"
                + ",START_DATE"
                + ",END_DATE"
                + ",ADICIONAL_DISCOUNT"
                + ",NO_RECEIPT"
                + ",FILLING_VALUE"
                + ",PRODUCT_CODE"
                + ",PRODUCT_DESCRIPTION"
                + ",VOLUME"
                + ") VALUES ('"
                + "P','"
                + "0000000000000','"
                + PAN + "','"
                + store.getNumber() + "','"
                + "'00000000,'"
                + actualDateInString + "','"
                + optNumber + "','"
                + transactionNumber + "','"
                + "000000','"
                + actualDate + "','"
                + actualDate + "','"
                + "000000','"
                + receiptRequested + "','"
                + fillingValue + "','"
                + productCode + "','"
                + productDescription + "','"
                + numberFormat2.format(Integer.parseInt(promoFormat.format(volume).replaceAll(",", ""))) + "')";
        System.out.println("Executing statment: " + statementToExecute);
        sqlConnection.executeStatement(statementToExecute);
        sqlConnection.disconnect();    
    }
    * 
    */
}
