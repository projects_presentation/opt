package MainPackage;

import FileLogger.FileLogger;
import javax.swing.UIManager;

public class Main {
    private static FileLogger fileLogger = new FileLogger("OPT", "SOFTWARE\\GWORKS\\OPT\\");

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            new MainWindow().setVisible(true);
        }
        catch (Exception ex) {
            fileLogger.logException(ex,fileLogger.ERROR);
        }
    }

}
