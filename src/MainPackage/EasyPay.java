package MainPackage;

import FileLogger.FileLogger;
import MicrelecPackage.Micrelec;

import RegistryManager.RegistryManager;
import TPAPackage.SecurityModule;
import VCPackage.VCJumbo;
import java.util.ArrayList;

public class EasyPay {

    private int numberOfSides = 0;
    private FileLogger logger;
    private SecurityModule securityModule;
    private Micrelec micrelec;
    private ArrayList<SecurityModule> listofSecurityModules;
    private RegistryManager registryManager;
    private final String REGISTRY_ROOT = "SOFTWARE\\GWorks\\OPT\\";

    private MainWindow mainWindow;

    public EasyPay(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        registryManager = new RegistryManager(REGISTRY_ROOT);
        this.numberOfSides = Integer.parseInt(registryManager.readValue("Settings", "Number of sides",false));
        startApplication();
    }

    private void startApplication() {
        logger = new FileLogger("OPT",REGISTRY_ROOT);
        logger.logString("Main application started",logger.INFO);
        listofSecurityModules = new ArrayList<SecurityModule>(this.numberOfSides);
        micrelec = new Micrelec(registryManager.readValue("Micrelec", "ComPort",false),listofSecurityModules,mainWindow);
        createSecurityModules();

    }

    private void createSecurityModules() {
        VCJumbo vCJumbo = new VCJumbo();
        for (int i = 0; i < numberOfSides; ++i) {
            securityModule = new SecurityModule(micrelec, i + 1,mainWindow,vCJumbo);
            listofSecurityModules.add(securityModule);
            logger.logString("Created Security for side: " + (i+1),logger.INFO);
        }
    }

    public SecurityModule getSecurityModel(int securityModelNumber){
        return listofSecurityModules.get(securityModelNumber-1);
    }
}
